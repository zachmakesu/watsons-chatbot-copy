Rails.application.routes.draw do
  mount API::Base => '/'
  mount Facebook::Messenger::Server, at: 'bot'
  mount Sidekiq::Web => "/sidekiq" # monitoring console

  get "registered_links/popularity/:id", to: "registered_links#popularity"

  devise_for :users, controllers: { sessions: "users/sessions", omniauth_callbacks: "users/omniauth_callbacks" }

  devise_scope :user do

    authenticated :user do

      root "home#index", as: :authenticated_root
      get "base/contact_admin", to: "base#contact_admin", as: "contact_admin"

      scope :analytics do
        get "/",                    to: "analytics#index",               as: "analytics"
        get "/user_retention",      to: "analytics#user_retention",      as: "user_retention"
        get "/:id",                 to: "analytics#page_analytics",      as: "page_analytics"
        get "/:id/daily_new_users",     to: "analytics#daily_new_users",     as: "daily_new_users"
        get "/:id/daily_users_actions", to: "analytics#daily_users_actions", as: "daily_users_actions"
        get "/:id/user_activity",       to: "analytics#user_activity",       as: "user_activity"
        #get "/:id/sources",             to: "analytics#sources",             as: "sources"
      end

      get "fb_pages", to: "fb_pages#index", as: "fb_pages"
      post "fb_pages/register", to: "fb_pages#register", as: "register"

      resources :users
      resources :registered_links

      delete "registered_pages/:id", to: "bot_records#destroy", as: "destroy_registered_page"
      post "registered_pages/:id/subscriptions", to: "bot_records#subscribe", as: "registered_page_subscribe"
      delete "registered_pages/:id/subscriptions", to: "bot_records#unsubscribe", as: "registered_page_unsubscribe"
    end
    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end
end
