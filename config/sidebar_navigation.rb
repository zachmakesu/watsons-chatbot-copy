SimpleNavigation::Configuration.run do |navigation|
  navigation.renderer = FoundationRenderer
  navigation.selected_class = 'active'
  navigation.autogenerate_item_ids = false
  navigation.items do |primary|
    if current_user.role == "super_admin"
      primary.item :users, 'Users', users_path, link_html: { class: "icon ion-android-contacts" }
      primary.item :registered_links, 'Registered Links', registered_links_path, link_html: { class: "icon ion-android-contacts" }
    elsif current_user.role == "admin"
      primary.item :fb_pages, 'BOT PUBLISHING', fb_pages_path, link_html: { class: "icon ion-ios-bookmarks" }
      #primary.item :bot_profile, 'BOT PROFILE', '#', link_html: { class: "icon ion-social-android" }
      primary.item :analytics, 'ANALYTICS', analytics_path, link_html: { class: "icon ion-connection-bars" }
    else

    end
    primary.dom_class = 'dropdown menu sidbar-menu'
  end
end
