SimpleNavigation::Configuration.run do |navigation|
  navigation.renderer = FoundationRenderer
  navigation.selected_class = 'active'
  navigation.autogenerate_item_ids = false
  navigation.items do |primary|
    if current_user.role == "super_admin"
      primary.item :users, 'USERS', users_path
    elsif current_user.role == "admin"
      primary.item :analytics, 'ANALYTICS', analytics_path
      #primary.item :bot_profile, 'BOT PROFILE', '#'
    else

    end

    primary.dom_class = 'dropdown menu main-menu'
  end
end
