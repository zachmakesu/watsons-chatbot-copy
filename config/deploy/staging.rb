set :branch, ENV.fetch("CAPISTRANO_BRANCH", "development")
set :mb_sidekiq_concurrency, 1
set :mb_privileged_user, "deployer"
set :deploy_to, "/home/deployer/apps/staging-watsons-chatbot"
set :stage, "staging"
server "deployer@staging-watsons-chatbot.southeastasia.cloudapp.azure.com",
       :user => "deployer",
       :roles => %w[app backup cron db redis sidekiq web]
