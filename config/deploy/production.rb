set :branch, ENV.fetch("CAPISTRANO_BRANCH", "master")
set :mb_sidekiq_concurrency, 1
set :mb_privileged_user, "deployer"
set :deploy_to, "/home/deployer/apps/production-watsons-chatbot"
set :stage, "production"
server "deployer@prod-watsons-chatbot.southeastasia.cloudapp.azure.com",
       :user => "deployer",
       :roles => %w[app backup cron db redis sidekiq web]
