# == Schema Information
#
# Table name: popularities
#
#  block_payload :string
#  bot_record_id :integer
#  category      :integer          not null
#  content       :text             not null
#  created_at    :datetime         not null
#  fb_obj_id     :integer          not null
#  id            :integer          not null, primary key
#  name          :string
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_popularities_on_bot_record_id  (bot_record_id)
#

class Popularity < ApplicationRecord
  belongs_to :bot_record
  enum category: { button: 0, block: 1, user_input: 2 , unrecognized_user_input: 3 }

  scope :between, ->(start_date, end_date) { where(created_at: start_date..end_date) }
  scope :group_by_fb_object_id_and_block_payload, -> { select('DISTINCT on (fb_obj_id, block_payload) *') }
  scope :group_by_fb_object_id_and_content, -> { select('DISTINCT on (fb_obj_id, content) *') }
  scope :per_payload, -> (payloads) { where(block_payload: payloads)}
  scope :except_get_started, -> { where.not(block_payload: "GET_STARTED_PAYLOAD") }
end
