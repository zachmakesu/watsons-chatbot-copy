# == Schema Information
#
# Table name: bot_records
#
#  access_token :text
#  created_at   :datetime         not null
#  id           :integer          not null, primary key
#  name         :string
#  page_id      :string
#  updated_at   :datetime         not null
#  verify_token :text
#

class BotRecord < ApplicationRecord
  has_many :bot_managements, dependent: :destroy
  has_many :managers, through: :bot_managements, source: :user
  has_many :popularities, dependent: :destroy
  has_many :sources, dependent: :destroy
  has_many :user_activities, dependent: :destroy
  has_many :popular_urls, dependent: :destroy

  validates :page_id, uniqueness: true
end
