# == Schema Information
#
# Table name: messenger_users
#
#  created_at      :datetime         not null
#  first_name      :string
#  id              :integer          not null, primary key
#  job_id          :string
#  last_name       :string
#  last_reply      :datetime
#  message_concern :string
#  sendable        :boolean          default(TRUE)
#  updated_at      :datetime         not null
#  user_fb_id      :integer          not null
#

class MessengerUser < ApplicationRecord

  scope :between, ->(start_date, end_date) { where(created_at: start_date..end_date) }
end
