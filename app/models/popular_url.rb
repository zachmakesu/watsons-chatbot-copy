# == Schema Information
#
# Table name: popular_urls
#
#  bot_record_id   :integer
#  created_at      :datetime         not null
#  fb_obj_id       :integer          not null
#  id              :integer          not null, primary key
#  name            :string           not null
#  registered_link :string           not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_popular_urls_on_bot_record_id  (bot_record_id)
#
# Foreign Keys
#
#  fk_rails_...  (bot_record_id => bot_records.id)
#

class PopularUrl < ApplicationRecord
  belongs_to :bot_record
  scope :between, ->(start_date, end_date) { where(created_at: start_date..end_date) }
end
