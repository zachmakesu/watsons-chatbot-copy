# == Schema Information
#
# Table name: sources
#
#  bot_record_id :integer
#  category      :integer          not null
#  created_at    :datetime         not null
#  fb_obj_id     :integer          not null
#  id            :integer          not null, primary key
#  ref           :string           not null
#  ref_source    :string
#  ref_type      :string
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_sources_on_bot_record_id  (bot_record_id)
#

class Source < ApplicationRecord
  belongs_to :bot_record
  enum category: { referral: 0, optin: 1 }

  scope :between, ->(start_date, end_date) { where(created_at: start_date..end_date) }
  scope :group_by_fb_object_id, -> { select('DISTINCT on (fb_obj_id, ref) *') }
end
