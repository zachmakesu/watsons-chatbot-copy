# == Schema Information
#
# Table name: user_activities
#
#  bot_record_id :integer
#  category      :string           not null
#  created_at    :datetime         not null
#  fb_obj_id     :integer          not null
#  id            :integer          not null, primary key
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_user_activities_on_bot_record_id  (bot_record_id)
#

class UserActivity < ApplicationRecord
  ACTIVITY_SORT_AREA = {
    active_user: ["Message", "Postback"],
    user_broad_cast_read: ["Read"],
    user_input_received: ["Message", "Postback"]
  }
  belongs_to :bot_record

  scope :between, ->(start_date, end_date) { where(created_at: start_date..end_date) }
  scope :sorted_by_area, ->(areas){ where(category:  areas) }
end
