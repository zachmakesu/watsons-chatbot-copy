# == Schema Information
#
# Table name: ai_payloaders
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  keyword    :text             not null
#  payload    :string           not null
#  updated_at :datetime         not null
#

class AiPayloader < ApplicationRecord
  validates :keyword, uniqueness: true

  scope :order_by_charlength_desc, -> { order("CHAR_LENGTH(keyword) desc") }

  #return the first occurence of payload. nil if not present
  def self.get_payload(text:)
    self.order_by_charlength_desc.detect do |word|
      searched = text.downcase.scan(word.keyword)
      searched.present?
    end
  end
end
