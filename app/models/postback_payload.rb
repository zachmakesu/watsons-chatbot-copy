# == Schema Information
#
# Table name: postback_payloads
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  payload    :string           not null
#  send_hash  :text             not null
#  updated_at :datetime         not null
#

class PostbackPayload < ApplicationRecord
end
