# == Schema Information
#
# Table name: bot_managements
#
#  bot_record_id :integer
#  created_at    :datetime         not null
#  id            :integer          not null, primary key
#  updated_at    :datetime         not null
#  user_id       :integer
#
# Indexes
#
#  index_bot_managements_on_bot_record_id  (bot_record_id)
#  index_bot_managements_on_user_id        (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (bot_record_id => bot_records.id)
#  fk_rails_...  (user_id => users.id)
#

class BotManagement < ApplicationRecord
  belongs_to :bot_record
  belongs_to :user
end
