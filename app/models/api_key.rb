# == Schema Information
#
# Table name: api_keys
#
#  created_at             :datetime         not null
#  encrypted_access_token :string           default(""), not null
#  expires_at             :datetime
#  id                     :integer          not null, primary key
#  updated_at             :datetime         not null
#  user_id                :integer
#
# Indexes
#
#  index_api_keys_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class ApiKey < ApplicationRecord
  scope :valid, -> { where('expires_at >= ?', DateTime.now) }
  before_create :set_expiration

  belongs_to :user

  validates_presence_of :access_token, :encrypted_access_token
  validates_length_of :access_token, minimum: 32

  def access_token
    if self.encrypted_access_token.blank?
      nil
    else
      @access_token ||= BCrypt::Password.new(self.encrypted_access_token)
    end

  end

  def access_token=(new_access_token)
    if new_access_token.blank?
      nil
    else
      @access_token = BCrypt::Password.create(new_access_token)
      self.encrypted_access_token = @access_token
    end
  end

  # Use devise method for comparing encrypted access token and param
  # http://www.rubydoc.info/github/plataformatec/devise/Devise.secure_compare
  def self.secure_compare(token_param, encrypted_token)
    return false if encrypted_token.blank?
    bcrypt  = ::BCrypt::Password.new(encrypted_token)
    token   = ::BCrypt::Engine.hash_secret(token_param, bcrypt.salt)

    a = token
    b = encrypted_token

    return false if a.blank? || b.blank? || a.bytesize != b.bytesize
    l = a.unpack "C#{a.bytesize}"

    res = 0
    b.each_byte { |byte| res |= byte ^ l.shift }
    res == 0
  end

  private
  def set_expiration
    self.expires_at = 14.days.from_now
  end

end
