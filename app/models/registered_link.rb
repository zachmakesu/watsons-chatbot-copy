# == Schema Information
#
# Table name: registered_links
#
#  content    :string
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  orig_url   :string           not null
#  title      :string
#  updated_at :datetime         not null
#

class RegisteredLink < ApplicationRecord
  validates :orig_url, :format => URI::regexp(%w(http https))
  validates :title, presence: true
  validates :title, uniqueness: true
end
