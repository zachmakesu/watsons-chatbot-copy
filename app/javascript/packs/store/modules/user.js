// Create a default const objects named state, mutations, actions and getters. and export it.
const state = {
  access_token: ''
};

const mutations = {
  'SET_USER' (state, user) {
    state.access_token = user.access_token;
  }
};

const actions = {
  setData: ( {commit} ) => {
    commit('SET_USER', user);
  },
};

const getters = {
  user: state => {
    return state;
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};