import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user';
import * as actions from './actions';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    errors: ''
  },
  actions,
  modules: {
    user
  }
});