import NotFound from './components/NotFound.vue'

const Bots = resolve => {
  require.ensure(['./components/bots/Bots.vue'],() => {
    resolve(require('./components/bots/Bots.vue'));
  });
};

export const routes = [
  {
    path: '/',
    name: 'Redirect',
    // vue-router lets us define a redirect method, the target route `to` is available for the redirect function
    // https://router.vuejs.org/en/essentials/redirect-and-alias.html
    redirect: function (to) {
      if (to.query.redirect) {
        // This will clear the ?redirect=<path> from the end URL
        var path = to.query.redirect
        delete to.query.redirect
        return {
          path: '/' + path,
          query: to.query
        }
      } else {
        return {
          path: '',
          query: to.query
        }
      }
    }
  },
  { 
    path: '/fb_bots',
    name: 'Bots',
    component: Bots
  },
  {
    path: '*',
    name: 'NotFound',
    component: NotFound
  }
];