/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb
window.fbAsyncInit = function() {
  FB.init({
    appId            : '112369026134754',
    autoLogAppEvents : true,
    xfbml            : true,
    version          : 'v2.10'
  });
  FB.AppEvents.logPageView();
};

(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "//connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));
 
import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './App.vue'
import store from './store/store'
import FBSignInButton from 'vue-facebook-signin-button'
import VueRouter from 'vue-router'
import { routes } from './routes'

Vue.use(FBSignInButton)
Vue.use(VueResource)
Vue.use(VueRouter)

Vue.http.options.root ='http://localhost:3000/api/v1/';

const router = new VueRouter({
  routes,
  mode: 'history'
});

document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
  })
})
