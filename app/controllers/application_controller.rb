class ApplicationController < ActionController::Base
  protect_from_forgery :with => :exception

  def something_went_wrong
    respond_to do |format|
      format.html { render template: 'errors/something_went_wrong_error', layout: 'layouts/application', status: 500 }
      format.all  { render nothing: true, status: 500 }
    end
  end

  def page_not_found
    respond_to do |format|
      format.html { render template: 'errors/page_not_found', layout: 'layouts/application', status: 404 }
      format.all  { render nothing: true, status: 404 }
    end
  end

  def ensure_admin
    redirect_to dynamic_path_for(current_user), alert: "Unauthorized access!" unless current_user.admin?
  end

  def ensure_super_admin
    redirect_to dynamic_path_for(current_user), alert: "Unauthorized access!" unless current_user.super_admin?
  end

  def redirect_normal_user
    redirect_to contact_admin_path, alert: "Please contact your Admin" if current_user.normal?
  end
end
