# Renders the home page.
class HomeController < ApplicationController
  before_action :redirect_normal_user

  # include Facebook::Messenger
  # before_action :set_pages
  # before_action :set_bot, only: [:subscribe, :unsubscribe, :test]

  def index
    if current_user.super_admin?
      redirect_to users_path
    else
      redirect_to fb_pages_path
    end
  end

  # def subscribe
  #   Facebook::Messenger::Subscriptions.subscribe(access_token: @bot.access_token)
  #   # PROFILE OF BOT FOR FIRST TIME
  #
  #   #unsetting of profile of bot
  #   # Facebook::Messenger::Profile.unset({
  #   #   fields: [
  #   #     "greeting",
  #   #     "get_started"
  #   #   ]
  #   # }, access_token: self.access_token )
  #
  #   #setting of profile of bot
  #   Facebook::Messenger::Profile.set({
  #     greeting: [
  #       {
  #         locale: 'default',
  #         text: "Hey, {{user_first_name}}! Are you ready to start the journey to having your best-skin-ever? Just click GET STARTED!",
  #       }
  #     ],
  #     get_started: {
  #       payload: 'GET_STARTED_PAYLOAD'
  #     },
  #     persistent_menu: [
  #       {
  #         locale: 'default',
  #         composer_input_disabled: false,
  #         call_to_actions: [
  #           {
  #             title: 'Menu',
  #             type: 'nested',
  #             call_to_actions: [
  #               {
  #                 title: "Call on Collagen",
  #                 type: 'postback',
  #                 payload: 'GET_STARTED_PAYLOAD'
  #               }
  #             ]
  #           },
  #           {
  #             type: "web_url",
  #             title:"Leave a Message",
  #             url: "http://m.me/150621454975420?ref=call_on_collagen"
  #           }
  #         ]
  #       }
  #     ]
  #   }, access_token: @bot.access_token )
  #
  # end
  #
  # def unsubscribe
  #   Facebook::Messenger::Subscriptions.unsubscribe(access_token: @bot.access_token)
  # end

  private

  # def set_pages
  #   @pages = if user_signed_in?
  #             current_user.bots
  #           else
  #             []
  #           end
  # end
  #
  # def set_bot
  #   @bot = BotRecord.find_by!(page_id: params[:page_id])
  # end

end
