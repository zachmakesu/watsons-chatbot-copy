class AnalyticsController < ApplicationController
  include AnalyticsHelper
  before_action :set_page, only: [ :page_analytics, :daily_users_actions, :daily_new_users, :user_activity ]
  before_action :set_koala, only: [ :page_analytics, :daily_users_actions, :daily_new_users, :user_activity ]

  def index
    @registered_pages = BotRecord.all
  end

  def page_analytics
    @registered_pages = BotRecord.all

    total_active_threads_unique = @active_threads_unique[0]["values"].map {|v| v["value"] }.sum

    other       = @unique_actions[0]["values"].map {|v| v["value"]["OTHER"] }.sum
    deleted     = @unique_actions[0]["values"].map {|v| v["value"]["DELETE"] }.sum
    report_spam = @unique_actions[0]["values"].map {|v| v["value"]["REPORT_SPAM"] }.sum
    turn_off    = @unique_actions[0]["values"].map {|v| v["value"]["TURN_OFF"] }.sum
    turn_on     = @unique_actions[0]["values"].map {|v| v["value"]["TURN_ON"] }.sum

    # total_active_threads_unique = 0
    # other = 0
    # deleted = 0
    # report_spam = 0
    # turn_off = 0
    # turn_on = 0
    # require "pry"
    # binding.pry
    @new_daily_users = new_daily_users_with_chart_dates
    @user_activities = user_activities_with_chart_dates

    @buttons = @page.popularities.button.between(@start_ts, @end_ts)
    @new_buttons = @buttons.map { |h| h.slice(:block_payload, :content, :name) }
    @unique_buttons = @new_buttons.reduce(Hash.new(0)) { |a, b| a[b] += 1; a }
    @unique_buttons = @unique_buttons.sort_by{ |v, k| -k }

    @unrecognized_user_inputs = @page.popularities.unrecognized_user_input.between(@start_ts, @end_ts)
    @new_unrecognized_user_inputs = @unrecognized_user_inputs.map { |h| h.slice(:content) }
    @unique_unrecognized_user_inputs = @new_unrecognized_user_inputs.reduce(Hash.new(0)) { |a, b| a[b] += 1; a }.sort_by{ |v, k| -k }

    @user_inputs = @page.popularities.user_input.between(@start_ts, @end_ts)
    @new_user_inputs = @user_inputs.map { |h| h.slice(:content) }
    @unique_user_inputs = @new_user_inputs.reduce(Hash.new(0)) { |a, b| a[b] += 1; a }.sort_by{ |v, k| -k }

    @blocks = @page.popularities.block.between(@start_ts, @end_ts)
    @new_blocks = @blocks.map { |h| h.slice(:block_payload, :name) }

    @unique_blocks = @new_blocks.reduce(Hash.new(0)) { |a, b| a[b] += 1; a }
    @unique_blocks = @unique_blocks.sort_by{ |v, k| -k }

    @popular_urls = @page.popular_urls.between(@start_ts, @end_ts)
    @new_popular_urls = @popular_urls.map { |h| h.slice(:name, :registered_link) }

    @unique_popular_urls = @new_popular_urls.reduce(Hash.new(0)) { |a, b| a[b] += 1; a }
    @unique_popular_urls = @unique_popular_urls.sort_by{ |v, k| -k }

    @sources = @page.sources.between(@start_ts, @end_ts)
    @new_sources = @sources.map { |h| h.slice(:category, :ref, :ref_source) }
    @unique_source_refs = @new_sources.reduce(Hash.new(0)) { |a, b| a[b] += 1; a }.sort_by{ |v, k| -k }

    @user_retention = user_retention_with_dates
    # binding.pry
  end

  def daily_users_actions
    actions = ['OTHER', 'DELETE', 'REPORT_SPAM', 'TURN_OFF', 'TURN_ON']
    @user_actions = actions.map{ |action| {name: action, data: @user_actions.each.map{ |k, v| ["#{Date.parse(k["end_time"])}", k["value"]["#{action}"]]}}}
    render json: @user_actions
  end

  def daily_new_users
    @new_daily_users = new_daily_users_with_chart_dates
    render json: @new_daily_users
  end

  def user_activity
    activities = ['active_users', 'user_broad_cast_read', 'user_input_received']
    @user_activities = user_activities_with_chart_dates
    @user_activities = activities.map{ |action| {name: action, data: @user_activities.each.map{ |k, v| [k, v[action.to_sym]]}}}
    render json: @user_activities
  end

  def user_retention
    @user_retention = user_retention_with_dates
  end

  private
  def set_page
    @page = BotRecord.find(params[:id])
  end

  def set_koala
    @graph = Koala::Facebook::API.new(@page.access_token)

    # @start_ts = "2017-09-20".to_date.beginning_of_day
    # @end_ts   = "2017-09-29".to_date.end_of_day

    if params[:start_ts].present? && params[:end_ts].present?
      @start_ts = params[:start_ts].to_datetime
      @end_ts   = params[:end_ts].to_datetime
    else
      @start_ts = DateTime.now.beginning_of_quarter
      @end_ts   = DateTime.now.end_of_quarter

      case params[:range_type]
      when "week"
        @start_ts = DateTime.now.beginning_of_week
        @end_ts   = DateTime.now.end_of_week
      when "month"
        @start_ts = DateTime.now.beginning_of_month
        @end_ts   = DateTime.now.end_of_month
      when "quarter"
        @start_ts = DateTime.now.beginning_of_quarter
        @end_ts   = DateTime.now.end_of_quarter
      else
        @start_ts = DateTime.now.beginning_of_week
        @end_ts   = DateTime.now.end_of_week
      end
    end

    @active_threads_unique = @graph.get_object("me/insights/page_messages_active_threads_unique", {since: @start_ts.to_i, until: @end_ts.to_i})
    @unique_actions = @graph.get_object("me/insights/page_messages_feedback_by_action_unique", {since: @start_ts.to_i, until: @end_ts.to_i})
    @user_actions = @unique_actions.first["values"]

    # @active_threads_unique = {}
    # @unique_actions = {}
    # @user_actions = {}
  end

  def user_activities_with_chart_dates
    activities   = @page.user_activities.between(@start_ts, @end_ts)
    active_users = @page.popularities.block.except_get_started.between(@start_ts, @end_ts)

    @new_collections = Hash.new
    (@start_ts..@end_ts).each do |d|
      user_a = activities.between(d.beginning_of_day, d.end_of_day)
      a_u = active_users.between(d.beginning_of_day, d.end_of_day)

      a_u_count     = a_u.select(:fb_obj_id).distinct.count
      u_b_c_r_count = user_a.sorted_by_area(UserActivity::ACTIVITY_SORT_AREA[:user_broad_cast_read]).select(:fb_obj_id).count
      u_i_r_count   = user_a.sorted_by_area(UserActivity::ACTIVITY_SORT_AREA[:user_input_received]).select(:fb_obj_id).count
      @new_collections[d] = {
        active_users: a_u_count,
        user_broad_cast_read: 0,
        user_input_received: u_i_r_count
      }
    end
    @new_collections
  end

  def new_daily_users_with_chart_dates
    collections =  @page.user_activities.between(@start_ts, @end_ts)
    @new_object_collections = Hash.new
    (@start_ts..@end_ts).each do |d|
      user_a = collections.between(d.beginning_of_day, d.end_of_day).group_by(&:fb_obj_id).collect{|k,v| v.first }
      reachable_user_count = user_a.count
      @new_object_collections[d] = reachable_user_count
    end
    @new_object_collections
  end

  def date_range_params
    params.permit(:start_ts, :end_ts)
  end

  def user_retention_with_dates
    @last_retention_date = if params[:last_retention_date].present?
      params[:last_retention_date].to_datetime rescue (DateTime.now - 1.days)
    else
      DateTime.now - 1.days
    end

    @last_14_days  = @last_retention_date - 14.days

    collections = MessengerUser.between(@last_14_days, @last_retention_date).order(created_at: :asc)
    @new_object_collections = Hash.new
    (@last_14_days..@last_retention_date).each do |date|

      messenger_users = collections.between(date.beginning_of_day, date.end_of_day).map { |u| u.user_fb_id }
      day_1 = user_retention_per_day(messenger_users: messenger_users, date: date, day: 1)
      day_2 = user_retention_per_day(messenger_users: messenger_users, date: date, day: 2)
      day_3 = user_retention_per_day(messenger_users: messenger_users, date: date, day: 3)
      day_4 = user_retention_per_day(messenger_users: messenger_users, date: date, day: 4)
      day_5 = user_retention_per_day(messenger_users: messenger_users, date: date, day: 5)
      day_6 = user_retention_per_day(messenger_users: messenger_users, date: date, day: 6)
      day_7 = user_retention_per_day(messenger_users: messenger_users, date: date, day: 7)

      @new_object_collections[date] = { users: messenger_users.count, day_1: day_1, day_2: day_2, day_3: day_3, day_4: day_4, day_5: day_5, day_6: day_6, day_7: day_7 }
    end
    @new_object_collections
  end

  def user_retention_per_day(messenger_users:, date:, day:)
    user_retention_count = UserActivity.between(date.beginning_of_day + day.days, date.end_of_day + day.days).where(fb_obj_id: messenger_users).select(:fb_obj_id).distinct.count

    percentage = user_retention_count == 0 ? 0 : (user_retention_count.to_f / messenger_users.count.to_f * 100.0).round(2)
    {percentage: percentage, count: user_retention_count}
  end
end
