class FbPagesController < ApplicationController
  before_action :ensure_admin

  def index
    @registered_pages = BotRecord.all

    @user_fb_pages = current_user.fb_graph.get_connections("me", "accounts")
    @fb_pages = @user_fb_pages.select{ |p| p if !@registered_pages.map(&:page_id).include?(p.fetch("id")) }

  end

  def register
    bot_record = BotRecord.new(bot_record_params)
    if bot_record.save
      redirect_to fb_pages_path, alert: "ERROR!"
    else
      redirect_to fb_pages_path
    end
  end

  def subscribe
    Chatbot::Subscribe.call(bot_record: @bot_record, page_id: params[:page_id], page_access_token: params[:page_access_token])
    redirect_to authenticated_root_path, notice: "Subscribed to #{params[:page_name]}"
  end

  def unsubscribe
    #Facebook::Messenger::Subscriptions.unsubscribe(access_token: @bot.access_token)
    redirect_to authenticated_root, notice: "Disconnected to"
  end

  private
    def bot_record_params
      {
        name: params[:bot_record][:name],
        access_token: params[:bot_record][:access_token],
        page_id: params[:bot_record][:id],
      }
    end
end
