# Renders the home page.
class UsersController < ApplicationController
  before_action :ensure_super_admin
  before_action :set_user, only: [ :update ]

  def index
    @users = User.except_self(current_user)
  end

  def update
    @user.update(user_params)
    # this resolves render nothing: true
    head :ok
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:role)
  end
end
