# Renders the home page.
class BotRecordsController < ApplicationController
  include Facebook::Messenger

  before_action :ensure_admin
  before_action :set_bot_record, only: [ :destroy, :subscribe, :unsubscribe ]

  def destroy
    @bot_record.destroy
    redirect_to fb_pages_path, alert: "Record removed!"
  end

  def subscribe
    Facebook::Messenger::Subscriptions.subscribe(access_token: @bot_record.access_token )
    redirect_to fb_pages_path, alert: "Subscribed!"
  end

  def unsubscribe
    Facebook::Messenger::Subscriptions.unsubscribe(access_token: @bot_record.access_token )
    redirect_to fb_pages_path, alert: "Unsubscribed!"
  end

  private

  def set_bot_record
    @bot_record = BotRecord.find(params[:id])
  end
end
