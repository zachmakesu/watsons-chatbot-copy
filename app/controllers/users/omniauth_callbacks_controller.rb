class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    response = FacebookSessionHandler.call(auth: request.env["omniauth.auth"])
    if response
      sign_in(response.user)
      redirect_to authenticated_root_path
    end
  end

  def failure
    set_flash_message :alert, :failure, kind: OmniAuth::Utils.camelize(failed_strategy.name), reason: failure_message
    redirect_to root_path
  end
end
