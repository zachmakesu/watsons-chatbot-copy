class RegisteredLinksController < ApplicationController
  before_action :ensure_super_admin, except: [:popularity]
  before_action :set_registered_link, only: [:destroy, :update, :edit]

  def index
    @registered_links = RegisteredLink.all.order(id: :asc)
  end

  def create
    @registered_link = RegisteredLink.new(registered_link_params)
    @registered_link.save
  end

  def edit

  end

  def update
    @registered_link.update(registered_link_params)
  end

  def destroy
    @registered_link.destroy
  end

  def popularity
    #routes > registered_links/popularity/:id?fb_obj_id=12312312321
    @registered_link = RegisteredLink.find_by(id: params[:id])
    if @registered_link
      # call on collagen page
      @fb_page = BotRecord.find(3)

      @fb_page.popular_urls.create(name: @registered_link.title, registered_link: @registered_link.orig_url, fb_obj_id: params[:fb_obj_id])
      redirect_to @registered_link.orig_url
    else
      # first data in RegisteredLink "FB WatsonsPH" => "https://www.messenger.com/t/WatsonsPH",
      redirect_to RegisteredLink.first.orig_url
    end

  end

  private
  def registered_link_params
    params.require(:registered_link).permit(:orig_url, :title)
  end

  def set_registered_link
    @registered_link = RegisteredLink.find(params[:id])
  end
end
