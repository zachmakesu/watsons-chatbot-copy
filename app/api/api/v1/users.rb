module API
  module V1
    class Users < Grape::API

      resource :profile do
        desc "User Profile"
        get do
          present current_user.decorate, with: Entities::V1::User::Profile, current_user: current_user
        end
      end
    end
  end
end
