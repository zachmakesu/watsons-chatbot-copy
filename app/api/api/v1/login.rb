module API
  module V1
    class Login < Grape::API
      resource :login do
        desc "Facebook Login"
        post do
          service = AuthenticateFbUser.call(
            token: params[:token]
          )
          if service.success?
            { 
              data: { 
                access_token: service.access_token,
                uid: service.uid
              } 
            }
          else
            error!(service.message, 400)
          end
        end
      end

    end
  end
end
