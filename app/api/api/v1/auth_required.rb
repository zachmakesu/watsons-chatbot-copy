module API
  module V1
    class AuthRequired < Grape::API

      helpers do
        def authenticate!
          #return if Rails.env.development?
          auth_header = headers['Authorization']

          # Check if Authorization header is present, else return 401
          error!("401 Error: Missing Authorization header", 401) if auth_header.blank?

          # Authorization header is present, check if it conforms to our specs
          error!("401 Error: Invalid Authorization header", 401) unless is_header_valid?(auth_header)

        end

        def is_header_valid? auth
          if /\AChatbot ([\w]+):([\w\+\=]+)\z/ =~ auth
            uid = $1
            signature = $2
            token = params[:access_token]
            # puts "\n####{request.path} params : #{params}\n###"
            # puts "#{request.path} is_token_valid? : #{is_token_valid?(uid, token)}"
            # puts "#{request.path} is_signature_authentic? : #{is_signature_authentic?(signature)}"
            return true #if Rails.env.development? || Rails.env.test?
            is_token_valid?(uid, token) && is_signature_authentic?(signature)
          else
            false
          end
        end

        def is_token_valid?(uid, token)
          if user = User.find_by(uid: uid)
            return api_key = user.api_keys.valid.detect{|a| ApiKey.secure_compare(token, a.encrypted_access_token) }
          else
            return false
          end
          ApiKey.secure_compare(token, api_key.encrypted_access_token)
        end

        def is_signature_authentic?(sig)
          generated_sig = HmacHandler.signature_from(request.path, params)
          Rails.logger.debug "#{request.path} expected: #{generated_sig}; actual: #{sig}" if Rails.env.staging?
          Rails.logger.debug "params: #{params}" if Rails.env.staging?
          HmacHandler.secure_compare(generated_sig, sig)
        end

        def current_user
          auth = headers['Authorization']
          # puts "request: #{request.path}"
          # puts "auth header: #{auth}"
          # puts "is_header_valid? #{is_header_valid?(auth)}"
          return nil unless is_header_valid? auth
          if /\AChatbot ([\w]+):([\w\+\=]+)\z/ =~ auth
            uid = $1
            # puts "uid: #{uid}"
            if @current_user
              @current_user
            else
              (@current_user = User.find_by(uid: uid)) ? @current_user : nil
            end
          end
        end
      end

      before do
        authenticate!
      end

      # Mount all endpoints that require authentication
      mount API::V1::Users
    end
  end
end
