module Entities
  module V1
    module User

      class Profile < Grape::Entity
        expose :data do
          expose :attributes do
            expose :first_name, :last_name, :email
          end
        end
        
      end

      class Index < Grape::Entity
        expose :data, using: Profile
        def data
          object.decorate
        end
      end

    end
  end
end
