class FacebookSessionHandler
  extend LightService::Organizer

  def self.call(auth:)
    with(auth: auth).reduce(actions)
  end

  def self.actions
    [
      #FETCH FB USER FROM GRAPH. returns context.user
      Actions::Session::FindOrCreateUser,
    ]
  end
end
