class WatsonsRouter
  include Rails.application.routes.url_helpers

  def self.default_url_options
    ActionController::Base.default_url_options
  end
end
