class Chatbot::MessageConcerns::AskEmail
  def self.replies(message_obj:, messenger_user:)
    #AI conditional
    messenger_user.update(sendable: true)
    messenger_user.update(message_concern: "ASK_MESSAGE_CONCERN")
    [
      { "text":"What is your message to my human friends?" }
    ]
  end
end
