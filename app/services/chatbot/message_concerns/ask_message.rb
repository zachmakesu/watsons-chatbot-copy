class Chatbot::MessageConcerns::AskMessage
  def self.replies(message_obj:, messenger_user:)
    #AI conditional
    messenger_user.update(sendable: true)
    messenger_user.update(message_concern: nil)
    [
      { "text":"Thank you for answering those questions. FYI, the questions you answered is just a brief demo of how you can use chatbots like me for lead generation. Ain't that cool?" }
    ]
  end
end
