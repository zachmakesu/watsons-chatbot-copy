class Chatbot::MessageConcerns::AskFullname
  def self.replies(message_obj:, messenger_user:)
    #AI conditional
    messenger_user.update(sendable: true)
    messenger_user.update(message_concern: "ASK_EMAIL_CONCERN")
    [
      { "text":"What is your email address?" }
    ]
  end
end
