class Chatbot::ProcessReferralObj
  extend LightService::Organizer

  def self.call(message_obj:)
    with(message_obj: message_obj).reduce(actions)
  end

  def self.actions
    [
      #GET PAGE. returns context.fb_page
      Actions::Chatbot::GetFbPage,

      #FETCH FB USER FROM GRAPH. returns context.fbuser_obj
      Actions::Chatbot::FetchFbUser,

      #REGISTER IT TO DATABASE MESSENGER USER. returns context.messenger_user
      Actions::Chatbot::FindOrCreateMessengerUser,

      #CREATE USER ACTIVITY
      Actions::Chatbot::CreateUserActivity,

      #CREATE SOURCE
      Actions::Chatbot::CreateSource,
    ]
  end
end
