class Chatbot::ProcessMessageObj
  extend LightService::Organizer

  def self.call(message_obj:)
    with(message_obj: message_obj).reduce(actions)
  end

  def self.actions
    [
      #GET PAGE. returns context.fb_page
      Actions::Chatbot::GetFbPage,

      #FETCH FB USER FROM GRAPH. returns context.fbuser_obj
      Actions::Chatbot::FetchFbUser,

      #REGISTER IT TO DATABASE MESSENGER USER. returns context.messenger_user
      Actions::Chatbot::FindOrCreateMessengerUser,

      #CREATE USER ACTIVITY
      Actions::Chatbot::CreateUserActivity,

      #DELETE WORKER THAT `SENDS MESSAGE` WHENEVER A USER RESPONSE BELOW 2MINS
      #Actions::Chatbot::DeletePayloadWorker,

      #CONDITION:
      # A MESSAGE_OBJ CAN ONLY BE A `NORMAL MESSAGE`, `QUICK REPLY` OR `HAS AN ATTACHMENTS`
      # A `QUICK REPLY` WILL MAKE messenger_user.concern to nil
      # A `NORMAL MESSAGE` AND `HAS AN ATTACHMENTS` may have messenger_user.concern

      #returns context.replies
      Actions::Chatbot::QuickReplyMessage,

      #returns context.replies
      Actions::Chatbot::AttachmentsMessage,

      #returns context.replies
      Actions::Chatbot::NormalMessage,

      #BOT WILL DELIVER context.replies
      Actions::Chatbot::DeliverReplies,

      #CREATE WORKER THAT `SENDS MESSAGE` WHENEVER A USER IS IDLE AFTER 2MINS
      #Actions::Chatbot::CreatePayloadWorker,
    ]
  end
end
