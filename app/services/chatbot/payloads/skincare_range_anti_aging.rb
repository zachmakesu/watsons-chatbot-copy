class Chatbot::Payloads::SkincareRangeAntiAging
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/9Na4rEJ.jpg'
          }
        }
      },
      { text: "Skin Renew reverses the signs of aging with Tri-Collagen and skin rejuvenation program which promises a firm and younger looking skin."},
      {
        attachment: {
          type: 'template',
          "payload": {
            "template_type": "generic",
            "image_aspect_ratio": "square",
            "elements": [
              {
                title: "Essence Water",
                subtitle: "SRP PHP 499.00",
                image_url: "https://i.imgur.com/IiJ0Ca1.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/17?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/4?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Day Lotion SPF 20",
                subtitle: "SRP PHP 549.00",
                image_url: "https://i.imgur.com/8J78Nx1.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/18?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/4?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Night Treatment Cream",
                subtitle: "SRP PHP 599.00",
                image_url: "https://i.imgur.com/8c8o2PT.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/4?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Intensive Serum",
                subtitle: "SRP PHP 549.00",
                image_url: "https://i.imgur.com/j4fROPQ.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/19?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/4?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Eye Treatment",
                subtitle: "SRP PHP 449.00",
                image_url: "https://i.imgur.com/nuNfMno.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/15?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/4?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "Skincare Range ⬅️",
            "payload": "SKINCARE_RANGE_PAYLOAD2"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
