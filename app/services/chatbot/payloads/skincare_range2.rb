class Chatbot::Payloads::SkincareRange2
  def self.replies(message_obj:, messenger_user:)
    [
      {
        text: 'Luckily, you came to the right place! Whatever your skin concern is, we have the perfect product for your needs! Choose among the range:',
        quick_replies: [
          {
            "content_type": "text",
            "title": "For Moisturising",
            "payload": "SKINCARE_RANGE_MOISTURISING"
          },
          {
            "content_type": "text",
            "title": "For Whitening",
            "payload": "SKINCARE_RANGE_WHITENING"
          },
          {
            "content_type": "text",
            "title": "For Anti-Aging",
            "payload": "SKINCARE_RANGE_ANTI_AGING"
          },
          {
            "content_type": "text",
            "title": "For Oily Skin",
            "payload": "SKINCARE_RANGE_OILY_SKIN"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
      
    ]
  end
end