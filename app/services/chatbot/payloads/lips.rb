class Chatbot::Payloads::Lips
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/lCeOMhJ.jpg'
          }
        }
      },
      { text: "Kiss dry and chaffed lips away with our lip care. Don’t snout and get ready to pout." },
      { text: "Collagen by Watsons offers simple and effective skincare. It is made with Tri-Collagen Complex that boosts the skin’s natural collagen levels deep down the layers." },
      {
        text: 'Soft, plump lips is a button away.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "LIPS_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Body Part ⬅️",
            "payload": "BODY_PART_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
