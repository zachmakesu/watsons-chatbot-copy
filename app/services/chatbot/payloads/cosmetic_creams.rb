class Chatbot::Payloads::CosmeticCreams
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/minsJS1.jpg'
          }
        }
      },
      { text: "Before applying makeup, cosmetic creams are handy as a primer or base. Using these products improve skin tone and texture." },
      {
        text: "Collagen by Watsons offers simple and effective skincare. It is made with Tri-Collagen Complex that boosts the skin’s natural collagen levels deep down the layers. Select which of the skincare range you’re interested in.",
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "COSMETIC_CREAMS_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "White Regeneration",
            "payload": "COSMETIC_CREAMS_WHITE_REGENERATION_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Product Type ⬅️",
            "payload": "PRODUCT_TYPE2_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
