class Chatbot::Payloads::IngestiblesDrink
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "Currently, Watsons offers Collagen ingestibles (capsules and ready-to- drink) through the Watsons Generics line." }
    ]
  end
end
