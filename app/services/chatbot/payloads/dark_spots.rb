class Chatbot::Payloads::DarkSpots
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/vOrESuv.jpg'
          }
        }
      },
      { text: "Ooof! Dark spots are the worst! It is common due to your daily exposure to the sun. But there is a solution that will make those dark spots say bye bye!" },
      { text: "Collagen by Watsons is a simple and effective skincare solution made with Tri-Collagen Complex. It boosts the skin’s natural collagen levels deep down the layers." },
      {
        text: 'We got the products to make your skin bright as day without the dark spots! Select which skincare range you’re interested in.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "White Regeneration",
            "payload": "DARK_SPOTS_WHITE_REGENERATION_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "DARK_SPOTS_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Concerns ⬅️",
            "payload": "SKIN_CONCERNS_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
