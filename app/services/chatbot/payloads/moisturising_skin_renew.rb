class Chatbot::Payloads::MoisturisingSkinRenew
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "Reverse the signs of aging with Skin Renew. It repairs and restore skin cells to give a firmer and younger-looking skin."},
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: [
              {
                title: "Day Lotion SPF 20",
                subtitle: "SRP PHP 549.00",
                image_url: "https://i.imgur.com/8J78Nx1.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/18?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/4?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Night Treatment Cream",
                subtitle: "SRP PHP 599.00",
                image_url: "https://i.imgur.com/8c8o2PT.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/4?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "MOISTURISING_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "White Regeneration",
            "payload": "MOISTURISING_WHITE_REGENERATION_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Hydro Balance",
            "payload": "MOISTURISING_HYDRO_BALANCE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Product Type ⬅️",
            "payload": "PRODUCT_TYPE2_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
