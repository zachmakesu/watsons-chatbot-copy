class Chatbot::Payloads::ProductType
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "Following a skincare regimen religiously is the key to beautiful skin." },
      {
        text: 'We know that it takes effort to have a beautiful skin so we’ll keep it simple for you.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Cleanser",
            "payload": "CLEANSING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Toners",
            "payload": "TONING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Moisturizers",
            "payload": "MOISTURISING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Cosmetic Creams",
            "payload": "COSMETIC_CREAMS_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Special Care",
            "payload": "SPECIAL_CARE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
