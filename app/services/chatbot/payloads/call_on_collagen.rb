class Chatbot::Payloads::CallOnCollagen
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "Call on Collagen is the chatbot of Collagen by Watsons to help you achieve a healthy, beautiful skin." },
      { text: "Which of the topics below would you like to learn more about?" },
      {
        attachment: {
          type: 'template',
          "payload": {
            "template_type": "list",
            "top_element_style": "compact",
            "elements": [
              {
                "title": "Skin Concerns",
                "buttons": [
                  {
                    "title": "Learn more",
                    "type": "postback",
                    "payload": 'SKIN_CONCERNS_PAYLOAD'
                  }
                ]
              },
              {
                "title": "Body Part",
                "buttons": [
                  {
                    "title": "Learn more",
                    "type": "postback",
                    "payload": 'BODY_PART_PAYLOAD'
                  }
                ]
              },
              {
                "title": "Product Type",
                "buttons": [
                  {
                    "title": "Learn more",
                    "type": "postback",
                    "payload": 'PRODUCT_TYPE_PAYLOAD'
                  }
                ]
              },
              {
                "title": "Skincare Range",
                "buttons": [
                  {
                    "title": "Learn more",
                    "type": "postback",
                    "payload": 'SKINCARE_RANGE_PAYLOAD'
                  }
                ]
              }
            ]
          }
        }
      }
    ]
  end
end
