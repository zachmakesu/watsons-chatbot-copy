class Chatbot::Payloads::Random
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment:{
          type: "template",
          payload: {
            template_type: "button",
            text: "🤖 Hi, #{messenger_user.first_name}! That concern seems to need a
more specific solution. Please click the button below to leave a message to a Watsons Health & Beauty Specialist to help you further! Thank you!!",
            buttons: [
              {
                type: "web_url",
                url: "#{ActionController::Base.asset_host}/registered_links/popularity/1?fb_obj_id=#{messenger_user.user_fb_id}",
                title: "Leave a message"
              }
            ]
          }
        }
      }
    ]
  end
end
