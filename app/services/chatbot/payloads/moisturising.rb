class Chatbot::Payloads::Moisturising
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/BOU99Eh.jpg'
          }
        }
      },
      { text: "Your skin gets thirsty too. Moisturizers keep the skin hydrated. They can also be used to prevent the skin from being too oily." },
      {
        text: "Collagen by Watsons offers simple and effective skincare. It is made with Tri-Collagen Complex that boosts the skin’s natural collagen levels deep down the layers. Choose which skincare range you’re interested in.",
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "MOISTURISING_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "White Regeneration",
            "payload": "MOISTURISING_WHITE_REGENERATION_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Renew",
            "payload": "MOISTURISING_SKIN_RENEW_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Hydro Balance",
            "payload": "MOISTURISING_HYDRO_BALANCE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Product Type ⬅️",
            "payload": "PRODUCT_TYPE2_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
