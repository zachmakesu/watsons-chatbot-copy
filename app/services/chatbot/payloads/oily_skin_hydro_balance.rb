class Chatbot::Payloads::OilySkinHydroBalance
  def self.replies(message_obj:, messenger_user:)
    [
      {text: "Hydro Balance is suitable for oily skin. It keeps the skin hydrated to make it soft, smooth. It leaves a fresh, dewy look on the skin."},
      {
        attachment: {
          type: 'template',
          "payload": {
            "template_type": "generic",
            "image_aspect_ratio": "square",
            "elements": [
              {
                "title": "Active Toner",
                "subtitle": "SRP PHP 499.00",
                "image_url": "https://i.imgur.com/6S3Qq17.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/3?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                "title": "Intensive Serum",
                "subtitle": "SRP PHP 499.00",
                "image_url": "https://i.imgur.com/3fQ78nL.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/3?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                "title": "Eye Treatment",
                "subtitle": "SRP PHP 299.00",
                "image_url": "https://i.imgur.com/jTpYFsA.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/3?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                "title": "Night Defense Cream",
                "subtitle": "SRP PHP 549.00",
                "image_url": "https://i.imgur.com/o6tJICc.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/3?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                "title": "Hydrating and Brightening Facial Mask",
                "subtitle": "SRP PHP 69.00",
                "image_url": "https://i.imgur.com/3WCHuw0.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/3?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "OILY_SKIN_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "White Regeneration",
            "payload": "OILY_SKIN_WHITE_REGENERATION_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Concerns ⬅️",
            "payload": "SKIN_CONCERNS_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
