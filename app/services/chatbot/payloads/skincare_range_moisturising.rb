class Chatbot::Payloads::SkincareRangeMoisturising
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/sj5q87k.jpg'
          }
        }
      },
      {text: "The Nourishing range has Tri-collagen that helps minimize wrinkles and fine lines, improves your skin’s elasticity, and purifies your skin."},
      {
        attachment: {
          type: 'template',
          "payload": {
            "template_type": "generic",
            "image_aspect_ratio": "square",
            "elements": [
              {
                title: "Moisturising & Firming Eye Roll On",
                subtitle: "SRP PHP 199.00",
                image_url: "https://i.imgur.com/blhEhII.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Moisturising & Firming Facial Cream",
                subtitle: "SRP PHP 549.00",
                image_url: "https://i.imgur.com/HDCcwVL.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/14?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Revitalising 10-in-1 Complexion Corrector Cream in Natural or Bright",
                subtitle: "SRP PHP 549.00",
                image_url: "https://i.imgur.com/PZOd9jI.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/11?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase (Natural)",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/12?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase (Bright)",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Moisturising & Brightening Facial Mask",
                subtitle: "SRP PHP 69.00",
                image_url: "https://i.imgur.com/lIQiqaG.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Intensive Nourishing Facial Mask",
                subtitle: "SRP PHP 69.00",
                image_url: "https://i.imgur.com/IH5BUL7.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/16?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Moisturising & Firming Toner 150ml",
                subtitle: "SRP PHP 349.00",
                image_url: "https://i.imgur.com/8wphyFy.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/13?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Q10 Intensive Moisturising & Firming Hand Cream",
                subtitle: "SRP PHP 119.00",
                image_url: "https://i.imgur.com/7Nwssug.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Moisturising & Repairing Cracked Heel Cream",
                subtitle: "SRP PHP 139.00",
                image_url: "https://i.imgur.com/lrYk2ZV.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Q10 Intensive Moisturising & Firming Body Lotion",
                subtitle: "SRP PHP 209.00",
                image_url: "https://i.imgur.com/GFzy76l.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/6?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "See more products",
                image_url: "https://i.imgur.com/PdzStyO.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "See more",
                    payload: "SKINCARE_RANGE_MOISTURISING_2"
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "Skincare Range ⬅️",
            "payload": "SKINCARE_RANGE_PAYLOAD2"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
