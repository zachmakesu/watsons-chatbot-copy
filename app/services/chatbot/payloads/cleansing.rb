class Chatbot::Payloads::Cleansing
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/JPCR6bJ.jpg'
          }
        }
      },
      { text: "Dirt, excess oil and dead skin build up can cause skin impurities. Wash them away by daily cleansing. Exfoliating at least twice a week also helps to keep the skin clear and fresh." },
      {
        text: 'Collagen by Watsons offers simple and effective skincare. It is made with Tri-Collagen Complex that boosts the skin’s natural collagen levels deep down the layers. Click on which skincare range you’re interested in.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "CLEANSING_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "White Regeneration",
            "payload": "CLEANSING_WHITE_REGENERATION_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Product Type ⬅️",
            "payload": "PRODUCT_TYPE2_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
