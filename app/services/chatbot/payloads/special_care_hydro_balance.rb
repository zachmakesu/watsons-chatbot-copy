class Chatbot::Payloads::SpecialCareHydroBalance
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "Hydro Balance is suitable for oily skin. It keeps the skin hydrated to make it soft, smooth. It leaves a fresh, dewy look on the skin."},
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: [
              {
                "title": "Eye Treatment",
                "subtitle": "SRP PHP 299.00",
                "image_url": "https://i.imgur.com/jTpYFsA.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/3?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                "title": "Intensive Serum",
                "subtitle": "SRP PHP 499.00",
                "image_url": "https://i.imgur.com/3fQ78nL.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/3?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "SPECIAL_CARE_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "White Regeneration",
            "payload": "SPECIAL_CARE_WHITE_REGENERATION_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Renew",
            "payload": "SPECIAL_CARE_SKIN_RENEW_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Product Type ⬅️",
            "payload": "PRODUCT_TYPE2_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
