class Chatbot::Payloads::SpecialCareNourishing
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "The Nourshing range deeply moisturizes the skin to make it smooth, firm and supple."},
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: [
              {
                title: "Moisturising & Firming Eye Roll On",
                subtitle: "SRP PHP 199.00",
                image_url: "https://i.imgur.com/blhEhII.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Q10 Intensive Moisturising & Firming Hand Cream",
                subtitle: "SRP PHP 119.00",
                image_url: "https://i.imgur.com/7Nwssug.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Moisturising & Repairing Cracked Heel Cream",
                subtitle: "SRP PHP 139.00",
                image_url: "https://i.imgur.com/lrYk2ZV.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Moisturising Lip Care Pack (Moisturising Lip Gel 3.5g + Moisturising Lip Balm 10g)",
                subtitle: "SRP PHP 249.00",
                image_url: "https://i.imgur.com/tMdpTAA.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "White Regeneration",
            "payload": "SPECIAL_CARE_WHITE_REGENERATION_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Renew",
            "payload": "SPECIAL_CARE_SKIN_RENEW_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Hydro Balance",
            "payload": "SPECIAL_CARE_HYDRO_BALANCE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Product Type ⬅️",
            "payload": "PRODUCT_TYPE2_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
