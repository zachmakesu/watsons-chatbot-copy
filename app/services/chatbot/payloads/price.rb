class Chatbot::Payloads::Price
  def self.replies(message_obj:, messenger_user:)
    [
      { "text":"Please select a product from the range to see the Standard Retail Price (SRP)." },
      {
        "attachment":{
          "type":"template",
          "payload":{
            "template_type":"list",
            "top_element_style":"compact",
            "elements":[
              {
                "title":"For Moisturising",
                "buttons":[
                  {
                    "title":"Learn more",
                    "type":"postback",
                    "payload":"SKINCARE_RANGE_MOISTURISING"
                  }
                ]
              },
              {
                "title":"For Whitening",
                "buttons":[
                  {
                    "title":"Learn more",
                    "type":"postback",
                    "payload":"SKINCARE_RANGE_WHITENING"
                  }
                ]
              },
              {
                "title":"For Anti-Aging",
                "buttons":[
                  {
                    "title":"Learn more",
                    "type":"postback",
                    "payload":"SKINCARE_RANGE_ANTI_AGING"
                  }
                ]
              },
              {
                "title":"For Oily Skin",
                "buttons":[
                  {
                    "title":"Learn more",
                    "type":"postback",
                    "payload":"SKINCARE_RANGE_OILY_SKIN"
                  }
                ]
              }
            ]
          }
        }
      }
    ]
  end
end
