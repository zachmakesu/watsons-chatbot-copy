class Chatbot::Payloads::OilySkin
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/h19bq2L.jpg',
          }
        }
      },
      { text: "Oil on our skin actually helps protect it from losing it’s youthful glow, however, excess oil is not such a good feeling and a good sight on our skin. You could use a good dose of cleansing and moisturizing! " },
      { text: "Collagen by Watsons is a simple and effective skincare solution made with Tri-Collagen Complex. It boosts the skin’s natural collagen levels deep down the layers." },
      {
        text: 'Wanna know what product will best keep your face oil- free? Select which skincare range you’re interested in.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Hydro Balance",
            "payload": "OILY_SKIN_HYDRO_BALANCE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "OILY_SKIN_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "White Regeneration",
            "payload": "OILY_SKIN_WHITE_REGENERATION_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Concerns ⬅️",
            "payload": "SKIN_CONCERNS_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
