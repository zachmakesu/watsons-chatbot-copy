class Chatbot::Payloads::Hands
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/R35sNs8.jpg'
          }
        }
      },
      { text: "Rough and dry palms? We know it’s tough. Collagen by Watsons offers simple and effective skin care. It is made with Tri- Collagen Complex that boosts the skin’s natural collagen levels deep down the layers." },
      {
        text: 'For softer and smoother hands to touch, click the button below.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "HANDS_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Body Part ⬅️",
            "payload": "BODY_PART_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
