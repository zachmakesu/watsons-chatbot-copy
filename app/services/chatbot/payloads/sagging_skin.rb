class Chatbot::Payloads::SaggingSkin
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/PG8Yen1.jpg'
          }
        }
      },
      { text: "There are many factors that contribute to our skin sagging, not just age. If you think there’s nothing you can do about it, you’re mistaken! Because collagen helps keep your skin firm so it won’t lose its natural elasticity." },
      { text: "Collagen by Watsons is a simple and effective skincare solution made with Tri-Collagen Complex. It boosts the skin’s natural collagen levels deep down the layers." },
      {
        text: 'Wanna know which Collagen products can help you get that firm and supple skin you deserve? Pick which skincare range you’re interested in.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Skin Renew",
            "payload": "SAGGING_SKIN_SKIN_RENEW_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "SAGGING_SKIN_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Concerns ⬅️",
            "payload": "SKIN_CONCERNS_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
