class Chatbot::Payloads::Body
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/kGwHemE.jpg'
          }
        }
      },
      { text: "May it be for firming or softening, we have the body lotion variants that deeply nourishes the skin." },
      { text: "Collagen by Watsons offers simple and effective skincare. It is made with Tri-Collagen Complex that boosts the skin’s natural collagen levels deep down the layers." },
      {
        text: 'Choose among our best sellers below.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "BODY_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Body Part ⬅️",
            "payload": "BODY_PART_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
