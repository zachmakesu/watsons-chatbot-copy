class Chatbot::Payloads::LinesAndWrinkles
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/qYJ1zVq.jpg'
          }
        }
      },
      { text: "Fine lines and wrinkles show as we age. But stress and poor habits can make our skin age faster. The good news is, you can do something to prevent them." },
      { text: "Collagen by Watsons is a simple and effective skincare solution made with Tri-Collagen Complex. It boosts the skin’s natural collagen levels deep down the layers." },
      {
        text: 'With proper hydration and moisturizing, your skin will look plumper and firmer in no time! And we have the best products to help you get the plum wrinkle-free skin! Click on which skincare range you’re interested in.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Skin Renew",
            "payload": "LINES_AND_WRINKLES_SKIN_RENEW_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "LINES_AND_WRINKLES_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Concerns ⬅️",
            "payload": "SKIN_CONCERNS_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
