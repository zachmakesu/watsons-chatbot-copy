class Chatbot::Payloads::LipsNourishing
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "The Nourshing range deeply moisturizes the skin to make it smooth, firm and supple."},
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: [
              {
                title: "Moisturising Lip Care Pack (Moisturising Lip Gel 3.5g + Moisturising Lip Balm 10g)",
                subtitle: "SRP PHP 249.00",
                image_url: "https://i.imgur.com/tMdpTAA.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "Body Part ⬅️",
            "payload": "BODY_PART_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
