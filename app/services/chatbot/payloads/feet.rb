class Chatbot::Payloads::Feet
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/jQKjRTH.jpg'
          }
        }
      },
      { text: "Cracked heels? Let’s heal those heels." },
      { text: "Collagen by Watsons offers simple and effective skin care. It is made with Tri-Collagen Complex that boosts the skin’s natural collagen levels deep down the layers." },
      {
        text: 'Find out more by clicking the button.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "FEET_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Body Part ⬅️",
            "payload": "BODY_PART_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
