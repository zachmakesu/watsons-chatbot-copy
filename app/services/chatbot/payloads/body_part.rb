class Chatbot::Payloads::BodyPart
  def self.replies(message_obj:, messenger_user:)
    [
      {
        text: 'Every part of our skin have specific needs. Collagen by Watsons has wide range that can cater to specific parts of your body.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Face 👩🏻",
            "payload": "FACE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Hands 👐",
            "payload": "HANDS_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Feet 👣",
            "payload": "FEET_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Body 💃🏻",
            "payload": "BODY_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Eye Area 👁",
            "payload": "EYE_AREA_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Lips 👄",
            "payload": "LIPS_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end