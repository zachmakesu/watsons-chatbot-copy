class Chatbot::Payloads::SkincareRangeWhitening
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/GZ2Zywg.jpg'
          }
        }
      },
      { text: "White Regeneration is your round-the-clock whitening solution with Tri-Collagen and Penta Lucent System to give you a fairer, glowing skin."},
      {
        attachment: {
          type: 'template',
          "payload": {
            "template_type": "generic",
            "image_aspect_ratio": "square",
            "elements": [
              {
                "title": "Cleansing Foam",
                "image_url": "https://i.imgur.com/9yyKPRu.jpg",
                "subtitle": "SRP PHP 329.00",
                "buttons": [
                  {
                    "type": "web_url",
                    "url": "#{ActionController::Base.asset_host}/registered_links/popularity/10?fb_obj_id=#{messenger_user.user_fb_id}",
                    "title": "Purchase",
                    "webview_height_ratio": "compact"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/5?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Toner",
                subtitle: "SRP PHP 499.00",
                image_url: "https://i.imgur.com/JqLoDtd.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/5?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Night Cream",
                subtitle: "SRP PHP 599.00",
                image_url: "https://i.imgur.com/MCWwFbe.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/5?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Instant Finishing Cream",
                subtitle: "SRP PHP 499.00",
                image_url: "https://i.imgur.com/PJpsEy0.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/5?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Bio-Cellulose Mask",
                subtitle: "SRP PHP 129.00",
                image_url: "https://i.imgur.com/LPVAD3f.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/5?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "CC Cream in Ivory or Nude",
                subtitle: "SRP PHP 299.00",
                image_url: "https://i.imgur.com/VmJsvNj.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/5?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "Skincare Range ⬅️",
            "payload": "SKINCARE_RANGE_PAYLOAD2"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
