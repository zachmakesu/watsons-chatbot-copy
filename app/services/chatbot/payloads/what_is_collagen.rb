class Chatbot::Payloads::WhatIsCollagen
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "Collagen is one of the skin's building block. It naturally occurs in our bodies to hold the skin cells together. Poor collagen levels in the skin causes skin concerns." }
    ]
  end
end
