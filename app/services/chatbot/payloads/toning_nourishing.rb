class Chatbot::Payloads::ToningNourishing
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "The Nourshing range deeply moisturizes the skin to make it smooth, firm and supple."},
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: [
              {
                title: "Moisturising & Firming Toner 150ml",
                subtitle: "SRP PHP 349.00",
                image_url: "https://i.imgur.com/8wphyFy.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/13?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "White Regeneration",
            "payload": "TONING_WHITE_REGENERATION_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Renew",
            "payload": "TONING_SKIN_RENEW_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Hydro Balance",
            "payload": "TONING_HYDRO_BALANCE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Product Type ⬅️",
            "payload": "PRODUCT_TYPE2_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
