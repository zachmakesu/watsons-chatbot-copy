class Chatbot::Payloads::Face
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/A5bCVZP.jpg'
          }
        }
      },
      { text: "From cleansing, exfoliating, toning to moisturising, Collagen by Watsons offers simple and effective care for the face. It is made with Tri-Collagen Complex that boosts the skin’s natural collagen levels deep down the layers." },
      {
        text: 'Choose among the wide range to find out the product that suit you.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "FACE_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "White Regeneration",
            "payload": "FACE_WHITE_REGENERATION_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Renew",
            "payload": "FACE_SKIN_RENEW_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Hydro Balance",
            "payload": "FACE_HYDRO_BALANCE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Body Part ⬅️",
            "payload": "BODY_PART_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
