class Chatbot::Payloads::RejuvenationProgram
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "Skin Renew's skin rejuvanation program includes:
1. Repairs damaged skin barrier

2. Restores skin moisture levels

3. Rebirths skin elasticity

4. Restarts skin renewal

5. Rebuilds your skin texture

6. Resumes plumpness of the skin" }
    ]
  end
end
