class Chatbot::Payloads::SkincareRange
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/puZy1BG.jpg'
          }
        }
      },
      { text: "Collagen by Watsons believes that beauty lies in the layers. Beauty is skin deep. So, it’s important to holistically care for your skin." },
      { text: "With the power of Tri-Collagen, Collagen by Watsons stimulates skin regeneration, locks in water for moisture, and nourishes and tightens skin, leaving you with youthful, firm, plump skin!" },
      {
        text: 'Luckily, you came to the right place! Whatever your skin concern is, we have the perfect product for your needs! Choose among the range:',
        quick_replies: [
          {
            "content_type": "text",
            "title": "For Moisturising",
            "payload": "SKINCARE_RANGE_MOISTURISING"
          },
          {
            "content_type": "text",
            "title": "For Whitening",
            "payload": "SKINCARE_RANGE_WHITENING"
          },
          {
            "content_type": "text",
            "title": "For Anti-Aging",
            "payload": "SKINCARE_RANGE_ANTI_AGING"
          },
          {
            "content_type": "text",
            "title": "For Oily Skin",
            "payload": "SKINCARE_RANGE_OILY_SKIN"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
      
    ]
  end
end