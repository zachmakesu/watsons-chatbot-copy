class Chatbot::Payloads::SkincareRangeOilySkin
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/ZpFO1Yh.jpg'
          }
        }
      },
      { text: "Hydro Balance gives you that dewy look. It keeps you looking fresh with Tri-Collagen and its Smart Water Channel that penetrates the skin deeply for a smooth, fresh-looking skin."},
      {
        attachment: {
          type: 'template',
          "payload": {
            "template_type": "generic",
            "image_aspect_ratio": "square",
            "elements": [
              {
                "title": "Active Toner",
                "subtitle": "SRP PHP 499.00",
                "image_url": "https://i.imgur.com/6S3Qq17.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/3?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                "title": "Intensive Serum",
                "subtitle": "SRP PHP 499.00",
                "image_url": "https://i.imgur.com/3fQ78nL.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/3?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                "title": "Eye Treatment",
                "subtitle": "SRP PHP 299.00",
                "image_url": "https://i.imgur.com/jTpYFsA.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/3?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                "title": "Night Defense Cream",
                "subtitle": "SRP PHP 549.00",
                "image_url": "https://i.imgur.com/o6tJICc.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/3?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                "title": "Hydrating and Brightening Facial Mask",
                "subtitle": "SRP PHP 69.00",
                "image_url": "https://i.imgur.com/3WCHuw0.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/3?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Moist Gel Cream",
                subtitle: "SRP PHP 459.00",
                image_url: "https://i.imgur.com/gSMhEdN.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/20?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/3?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "Skincare Range ⬅️",
            "payload": "SKINCARE_RANGE_PAYLOAD2"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
