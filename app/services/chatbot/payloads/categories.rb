class Chatbot::Payloads::Categories
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'template',
          "payload": {
            "template_type": "list",
            "top_element_style": "compact",
            "elements": [
              {
                "title": "Skin Concern",
                "buttons": [
                  {
                    "title": "Learn more",
                    "type": "postback",
                    "payload": 'SKIN_CONCERNS_PAYLOAD'
                  }
                ]
              },
              {
                "title": "Body Part",
                "buttons": [
                  {
                    "title": "Learn more",
                    "type": "postback",
                    "payload": 'BODY_PART_PAYLOAD'
                  }
                ]
              },
              {
                "title": "Product Type",
                "buttons": [
                  {
                    "title": "Learn more",
                    "type": "postback",
                    "payload": 'PRODUCT_TYPE_PAYLOAD'
                  }
                ]
              },
              {
                "title": "Skin Care Range",
                "buttons": [
                  {
                    "title": "Learn more",
                    "type": "postback",
                    "payload": 'SKINCARE_RANGE_PAYLOAD'
                  }
                ]
              }
            ]
          }
        }
      }
    ]
  end
end
