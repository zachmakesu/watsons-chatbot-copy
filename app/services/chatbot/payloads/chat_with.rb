class Chatbot::Payloads::ChatWith
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment:{
          type: "template",
          payload: {
            template_type: "button",
            text: 'You may get in touch with Watsons Health & Beauty specialist by clicking the "Leave a Message” button below.',
            buttons: [
              {
                type: "web_url",
                url: "#{ActionController::Base.asset_host}/registered_links/popularity/1?fb_obj_id=#{messenger_user.user_fb_id}",
                title: "Leave a message"
              }
            ]
          }
        }
      }
    ]
  end
end
