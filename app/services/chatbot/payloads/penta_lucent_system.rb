class Chatbot::Payloads::PentaLucentSystem
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "White Regeneration uses Penta Lucent System that gives all-round protection to prevent melanin formation and dullness." }
    ]
  end
end
