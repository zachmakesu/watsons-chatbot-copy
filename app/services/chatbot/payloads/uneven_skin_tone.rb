class Chatbot::Payloads::UnevenSkinTone
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/JgL5M7E.jpg'
          }
        }
      },
      { text: "We all want bright skin that’s fair and even, right? But uneven skin tone caused by pigmentation is almost unavoidable especially for people who are exposed to the sun a lot." },
      { text: "Collagen by Watsons is a simple and effective skincare solution made with Tri-Collagen Complex. It boosts the skin’s natural collagen levels deep down the layers." },
      {
        text: 'Lighten up and reveal even skin tone. Select which skincare range you’re interested in.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "White Regeneration",
            "payload": "UNEVEN_SKIN_TONE_WHITE_REGENERATION_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "UNEVEN_SKIN_TONE_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Concerns ⬅️",
            "payload": "SKIN_CONCERNS_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
