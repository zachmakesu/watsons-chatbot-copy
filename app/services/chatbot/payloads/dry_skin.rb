class Chatbot::Payloads::DrySkin
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/3RqY1wF.jpg'
          }
        }
      },
      { text: "Dry skin, huh? Looks like your skin needs ultimate nourishment. Collagen is a building block of a beautiful skin. When collagen levels are boosted, the more hydrated your skin becomes." },
      { text: "Collagen by Watsons is a simple and effective skincare solution made with Tri-Collagen Complex. It boosts the skin’s natural collagen levels deep down the layers." },
      {
        text: 'Wanna meet the perfect product to hydrate your skin? Choose which skincare range you’re interested in.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "DRY_SKIN_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Hydro Balance",
            "payload": "DRY_SKIN_HYDRO_BALANCE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Concerns ⬅️",
            "payload": "SKIN_CONCERNS_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
