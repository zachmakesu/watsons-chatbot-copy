class Chatbot::Payloads::SmartWaterChannel
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "Hydro Balance has Smart Water Channel that actively infuses water deeply into the skin to keep it moisturized." }
    ]
  end
end
