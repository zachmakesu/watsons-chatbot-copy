class Chatbot::Payloads::EyeArea
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/F5MRKIV.jpg'
          }
        }
      },
      { text: "Wake up without puffy eyes or panda eyes! Soothe the eye area with these treatments and you’ll be smizing in no time!" },
      { text: "Collagen by Watsons offers simple and effective skincare. It is made with Tri-Collagen Complex that boosts the skin’s natural collagen levels deep down the layers." },
      {
        text: 'Select which skincare range you’re interested in.',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "EYE_AREA_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Renew",
            "payload": "EYE_AREA_SKIN_RENEW_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Hydro Balance",
            "payload": "EYE_AREA_HYDRO_BALANCE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Body Part ⬅️",
            "payload": "BODY_PART_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
