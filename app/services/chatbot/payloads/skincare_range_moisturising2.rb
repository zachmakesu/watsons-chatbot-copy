class Chatbot::Payloads::SkincareRangeMoisturising2
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'template',
          "payload": {
            "template_type": "generic",
            "image_aspect_ratio": "square",
            "elements": [
              {
                title: "Moisturising & Softening Body Lotion",
                subtitle: "SRP PHP 209.00",
                image_url: "https://i.imgur.com/M00vQeL.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/7?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Moisturising Lip Care Pack (Moisturising Lip Gel 3.5g + Moisturising Lip Balm 10g)",
                subtitle: "SRP PHP 249.00",
                image_url: "https://i.imgur.com/tMdpTAA.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Purifying Foam Cleanser",
                subtitle: "SRP PHP 249.00",
                image_url: "https://i.imgur.com/bEm85lW.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/8?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Brightening Peeling Cleanser",
                subtitle: "SRP PHP 349.00",
                image_url: "https://i.imgur.com/2LcoWJO.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/9?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Previous products",
                image_url: "https://i.imgur.com/PdzStyO.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "See previous items",
                    payload: "SKINCARE_RANGE_MOISTURISING"
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "Skincare Range ⬅️",
            "payload": "SKINCARE_RANGE_PAYLOAD2"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
