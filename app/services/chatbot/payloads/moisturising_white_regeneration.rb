class Chatbot::Payloads::MoisturisingWhiteRegeneration
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "White Regeneration is round-the-clock whitening range. Skin becomes whiter, fairer and more translucent."},
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: [
              {
                title: "Night Cream",
                subtitle: "SRP PHP 599.00",
                image_url: "https://i.imgur.com/MCWwFbe.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/5?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "MOISTURISING_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Renew",
            "payload": "MOISTURISING_SKIN_RENEW_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Hydro Balance",
            "payload": "MOISTURISING_HYDRO_BALANCE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Product Type ⬅️",
            "payload": "PRODUCT_TYPE2_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
