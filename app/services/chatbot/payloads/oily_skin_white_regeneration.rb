class Chatbot::Payloads::OilySkinWhiteRegeneration
  def self.replies(message_obj:, messenger_user:)
    [
      {text: "White Regeneration is round-the-clock whitening range. Skin becomes whiter, fairer and more translucent."},
      {
        attachment: {
          type: 'template',
          "payload": {
            "template_type": "generic",
            "image_aspect_ratio": "square",
            "elements": [
              {
                "title": "Cleansing Foam",
                "image_url": "https://i.imgur.com/9yyKPRu.jpg",
                "subtitle": "SRP PHP 329.00",
                "buttons": [
                  {
                    "type": "web_url",
                    "url": "#{ActionController::Base.asset_host}/registered_links/popularity/10?fb_obj_id=#{messenger_user.user_fb_id}",
                    "title": "Purchase",
                    "webview_height_ratio": "compact"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/5?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "OILY_SKIN_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Hydro Balance",
            "payload": "OILY_SKIN_HYDRO_BALANCE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Concerns ⬅️",
            "payload": "SKIN_CONCERNS_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
