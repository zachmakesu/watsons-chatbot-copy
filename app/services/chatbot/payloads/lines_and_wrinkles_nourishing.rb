class Chatbot::Payloads::LinesAndWrinklesNourishing
  def self.replies(message_obj:, messenger_user:)
    [
      { text: "The Nourshing range deeply moisturizes the skin to make it smooth, firm and supple."},
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: [
              {
                title: "Moisturising & Firming Eye Roll On",
                subtitle: "SRP PHP 199.00",
                image_url: "https://i.imgur.com/blhEhII.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Moisturising & Firming Facial Cream",
                subtitle: "SRP PHP 549.00",
                image_url: "https://i.imgur.com/HDCcwVL.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/14?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Revitalising 10-in-1 Complexion Corrector Cream in Natural or Bright",
                subtitle: "SRP PHP 549.00",
                image_url: "https://i.imgur.com/PZOd9jI.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/11?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase (Natural)",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/12?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase (Bright)",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Moisturising & Brightening Facial Mask",
                subtitle: "SRP PHP 69.00",
                image_url: "https://i.imgur.com/lIQiqaG.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              },
              {
                title: "Intensive Nourishing Facial Mask",
                subtitle: "SRP PHP 69.00",
                image_url: "https://i.imgur.com/IH5BUL7.jpg",
                buttons: [
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/16?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Purchase",
                    webview_height_ratio: "full"
                  },
                  {
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/registered_links/popularity/2?fb_obj_id=#{messenger_user.user_fb_id}",
                    title: "Learn more",
                  }
                ]
              }
            ]
          }
        },
        quick_replies: [
          {
            "content_type": "text",
            "title": "Skin Renew",
            "payload": "LINES_AND_WRINKLES_SKIN_RENEW_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Concerns ⬅️",
            "payload": "SKIN_CONCERNS_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
