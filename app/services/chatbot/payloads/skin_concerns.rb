class Chatbot::Payloads::SkinConcerns
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/uEHykZB.jpg'
          }
        }
      },
      { text: "Collagen is your skin’s building block. It is important to replenish collagen in your skin to keep it nourished." },
      {
        text: 'Whatever your skin concern, Collagen by Watsons got you covered. Anything specific?',
        quick_replies: [
          {
            "content_type": "text",
            "title": "Oily Skin",
            "payload": "OILY_SKIN_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Dry Skin",
            "payload": "DRY_SKIN_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Dark Spots",
            "payload": "DARK_SPOTS_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Sagging Skin",
            "payload": "SAGGING_SKIN_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Lines & Wrinkles",
            "payload": "LINES_AND_WRINKLES_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Uneven Skin tone",
            "payload": "UNEVEN_SKIN_TONE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
