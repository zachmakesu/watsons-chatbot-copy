class Chatbot::Payloads::SpecialCare
  def self.replies(message_obj:, messenger_user:)
    [
      {
        attachment: {
          type: 'image',
          payload: {
            url: 'https://i.imgur.com/t6rnXre.jpg'
          }
        }
      },
      { text: "Caring for skin should not be limited to your face. Other parts of the body deserves special treatment too." },
      {
        text: "Collagen by Watsons has a wide range of special care products for the eye area, lips, hands and feet. It is made with Tri- Collagen Complex that boosts the skin’s natural collagen levels deep down the layers. Select which skincare range suits your interest.",
        quick_replies: [
          {
            "content_type": "text",
            "title": "Nourishing",
            "payload": "SPECIAL_CARE_NOURISHING_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "White Regeneration",
            "payload": "SPECIAL_CARE_WHITE_REGENERATION_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Skin Renew",
            "payload": "SPECIAL_CARE_SKIN_RENEW_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Hydro Balance",
            "payload": "SPECIAL_CARE_HYDRO_BALANCE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Product Type ⬅️",
            "payload": "PRODUCT_TYPE2_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Categories ⬅️",
            "payload": "CATEGORIES_PAYLOAD"
          }
        ]
      }
    ]
  end
end
