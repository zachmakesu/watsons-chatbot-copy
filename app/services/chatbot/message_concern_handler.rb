class Chatbot::MessageConcernHandler
  def self.response_from(message_obj:, messenger_user:)
    case messenger_user.message_concern
    when 'ASK_FULLNAME_CONCERN'         then Chatbot::MessageConcerns::AskFullname.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'ASK_EMAIL_CONCERN'            then Chatbot::MessageConcerns::AskEmail.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'ASK_MESSAGE_CONCERN'          then Chatbot::MessageConcerns::AskMessage.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'CHECK_CONCERN'                then [ {text: "Nice test"} ]
    else
    []
    end
  end
end
