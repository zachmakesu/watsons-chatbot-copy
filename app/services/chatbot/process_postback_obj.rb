class Chatbot::ProcessPostbackObj
  extend LightService::Organizer

  def self.call(message_obj:)
    with(message_obj: message_obj).reduce(actions)
  end

  def self.actions
    [
      #GET PAGE. returns context.fb_page
      Actions::Chatbot::GetFbPage,

      #FETCH FB USER FROM GRAPH. returns context.fbuser_obj
      Actions::Chatbot::FetchFbUser,

      #REGISTER IT TO DATABASE MESSENGER USER. returns context.messenger_user
      Actions::Chatbot::FindOrCreateMessengerUser,

      #CREATE USER ACTIVITY
      Actions::Chatbot::CreateUserActivity,

      #CREATE SOURCE
      Actions::Chatbot::CreateSource,

      #DELETE WORKER THAT `SENDS MESSAGE` WHENEVER A USER RESPONSE BELOW 2MINS
      #Actions::Chatbot::DeletePayloadWorker,

      #Every postback delete messenger_user.message_concern
      Actions::Chatbot::DeleteMessageConcern,

      #returns context.replies and context.block_name
      Actions::Chatbot::PayloadMessage,

      #CREATE POPULARITY
      Actions::Chatbot::CreatePostbackPopularity,

      #BOT WILL DELIVER context.replies
      Actions::Chatbot::DeliverReplies,

      #CREATE WORKER THAT `SENDS MESSAGE` WHENEVER A USER IS IDLE AFTER 2MINS
      #Actions::Chatbot::CreatePayloadWorker,
    ]
  end
end
