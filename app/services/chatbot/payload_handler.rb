class Chatbot::PayloadHandler
  def self.response_from(message_obj:, payload:, messenger_user:)
    case payload
    when 'GET_STARTED_PAYLOAD'          then Chatbot::Payloads::GetStarted.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'SKIN_CONCERNS_PAYLOAD'        then Chatbot::Payloads::SkinConcerns.replies(message_obj: message_obj, messenger_user: messenger_user)
      when 'OILY_SKIN_PAYLOAD'              then Chatbot::Payloads::OilySkin.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'OILY_SKIN_HYDRO_BALANCE_PAYLOAD'              then Chatbot::Payloads::OilySkinHydroBalance.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'OILY_SKIN_NOURISHING_PAYLOAD'                 then Chatbot::Payloads::OilySkinNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'OILY_SKIN_WHITE_REGENERATION_PAYLOAD'         then Chatbot::Payloads::OilySkinWhiteRegeneration.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'DRY_SKIN_PAYLOAD'               then Chatbot::Payloads::DrySkin.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'DRY_SKIN_NOURISHING_PAYLOAD'                  then Chatbot::Payloads::DrySkinNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'DRY_SKIN_HYDRO_BALANCE_PAYLOAD'               then Chatbot::Payloads::DrySkinHydroBalance.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'DARK_SPOTS_PAYLOAD'             then Chatbot::Payloads::DarkSpots.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'DARK_SPOTS_WHITE_REGENERATION_PAYLOAD'        then Chatbot::Payloads::DarkSpotsWhiteRegeneration.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'DARK_SPOTS_NOURISHING_PAYLOAD'                then Chatbot::Payloads::DarkSpotsNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'SAGGING_SKIN_PAYLOAD'           then Chatbot::Payloads::SaggingSkin.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'SAGGING_SKIN_SKIN_RENEW_PAYLOAD'              then Chatbot::Payloads::SaggingSkinSkinRenew.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'SAGGING_SKIN_NOURISHING_PAYLOAD'              then Chatbot::Payloads::SaggingSkinNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'LINES_AND_WRINKLES_PAYLOAD'     then Chatbot::Payloads::LinesAndWrinkles.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'LINES_AND_WRINKLES_SKIN_RENEW_PAYLOAD'        then Chatbot::Payloads::LinesAndWrinklesSkinRenew.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'LINES_AND_WRINKLES_NOURISHING_PAYLOAD'        then Chatbot::Payloads::LinesAndWrinklesNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'UNEVEN_SKIN_TONE_PAYLOAD'       then Chatbot::Payloads::UnevenSkinTone.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'UNEVEN_SKIN_TONE_WHITE_REGENERATION_PAYLOAD'  then Chatbot::Payloads::UnevenSkinToneWhiteRegeneration.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'UNEVEN_SKIN_TONE_NOURISHING_PAYLOAD'          then Chatbot::Payloads::UnevenSkinToneNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)

    when 'BODY_PART_PAYLOAD'            then Chatbot::Payloads::BodyPart.replies(message_obj: message_obj, messenger_user: messenger_user)
      when 'FACE_PAYLOAD'                  then Chatbot::Payloads::Face.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'FACE_NOURISHING_PAYLOAD'                      then Chatbot::Payloads::FaceNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'FACE_WHITE_REGENERATION_PAYLOAD'              then Chatbot::Payloads::FaceWhiteRegeneration.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'FACE_SKIN_RENEW_PAYLOAD'                      then Chatbot::Payloads::FaceSkinRenew.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'FACE_HYDRO_BALANCE_PAYLOAD'                   then Chatbot::Payloads::FaceHydroBalance.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'HANDS_PAYLOAD'                 then Chatbot::Payloads::Hands.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'HANDS_NOURISHING_PAYLOAD'                     then Chatbot::Payloads::HandsNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'FEET_PAYLOAD'                 then Chatbot::Payloads::Feet.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'FEET_NOURISHING_PAYLOAD'                     then Chatbot::Payloads::FeetNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'BODY_PAYLOAD'                 then Chatbot::Payloads::Body.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'BODY_NOURISHING_PAYLOAD'                     then Chatbot::Payloads::BodyNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'EYE_AREA_PAYLOAD'             then Chatbot::Payloads::EyeArea.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'EYE_AREA_NOURISHING_PAYLOAD'                 then Chatbot::Payloads::EyeAreaNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'EYE_AREA_SKIN_RENEW_PAYLOAD'                 then Chatbot::Payloads::EyeAreaSkinRenew.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'EYE_AREA_HYDRO_BALANCE_PAYLOAD'              then Chatbot::Payloads::EyeAreaHydroBalance.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'LIPS_PAYLOAD'                 then Chatbot::Payloads::Lips.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'LIPS_NOURISHING_PAYLOAD'                     then Chatbot::Payloads::LipsNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)

    when 'REGIMEN_PAYLOAD'                then Chatbot::Payloads::Regimen.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'REGIMEN2_PAYLOAD'               then Chatbot::Payloads::Regimen2.replies(message_obj: message_obj, messenger_user: messenger_user)

    when 'PRODUCT_TYPE_PAYLOAD'           then Chatbot::Payloads::ProductType.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'PRODUCT_TYPE2_PAYLOAD'          then Chatbot::Payloads::ProductType2.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'CLEANSING_PAYLOAD'            then Chatbot::Payloads::Cleansing.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'CLEANSING_NOURISHING_PAYLOAD'                 then Chatbot::Payloads::CleansingNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'CLEANSING_WHITE_REGENERATION_PAYLOAD'         then Chatbot::Payloads::CleansingWhiteRegeneration.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'TONING_PAYLOAD'               then Chatbot::Payloads::Toning.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'TONING_NOURISHING_PAYLOAD'                    then Chatbot::Payloads::ToningNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'TONING_WHITE_REGENERATION_PAYLOAD'            then Chatbot::Payloads::ToningWhiteRegeneration.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'TONING_SKIN_RENEW_PAYLOAD'                    then Chatbot::Payloads::ToningSkinRenew.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'TONING_HYDRO_BALANCE_PAYLOAD'                 then Chatbot::Payloads::ToningHydroBalance.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'MOISTURISING_PAYLOAD'         then Chatbot::Payloads::Moisturising.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'MOISTURISING_NOURISHING_PAYLOAD'              then Chatbot::Payloads::MoisturisingNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'MOISTURISING_WHITE_REGENERATION_PAYLOAD'      then Chatbot::Payloads::MoisturisingWhiteRegeneration.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'MOISTURISING_SKIN_RENEW_PAYLOAD'              then Chatbot::Payloads::MoisturisingSkinRenew.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'MOISTURISING_HYDRO_BALANCE_PAYLOAD'           then Chatbot::Payloads::MoisturisingHydroBalance.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'COSMETIC_CREAMS_PAYLOAD'      then Chatbot::Payloads::CosmeticCreams.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'COSMETIC_CREAMS_NOURISHING_PAYLOAD'           then Chatbot::Payloads::CosmeticCreamsNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'COSMETIC_CREAMS_WHITE_REGENERATION_PAYLOAD'   then Chatbot::Payloads::CosmeticCreamsWhiteRegeneration.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'SPECIAL_CARE_PAYLOAD'         then Chatbot::Payloads::SpecialCare.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'SPECIAL_CARE_NOURISHING_PAYLOAD'              then Chatbot::Payloads::SpecialCareNourishing.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'SPECIAL_CARE_WHITE_REGENERATION_PAYLOAD'      then Chatbot::Payloads::SpecialCareWhiteRegeneration.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'SPECIAL_CARE_SKIN_RENEW_PAYLOAD'              then Chatbot::Payloads::SpecialCareSkinRenew.replies(message_obj: message_obj, messenger_user: messenger_user)
        when 'SPECIAL_CARE_HYDRO_BALANCE_PAYLOAD'           then Chatbot::Payloads::SpecialCareHydroBalance.replies(message_obj: message_obj, messenger_user: messenger_user)

    when 'SKINCARE_RANGE_PAYLOAD'       then Chatbot::Payloads::SkincareRange.replies(message_obj: message_obj, messenger_user: messenger_user)
      when 'SKINCARE_RANGE_MOISTURISING'    then Chatbot::Payloads::SkincareRangeMoisturising.replies(message_obj: message_obj, messenger_user: messenger_user)
      when 'SKINCARE_RANGE_MOISTURISING_2'  then Chatbot::Payloads::SkincareRangeMoisturising2.replies(message_obj: message_obj, messenger_user: messenger_user)

      when 'SKINCARE_RANGE_WHITENING'       then Chatbot::Payloads::SkincareRangeWhitening.replies(message_obj: message_obj, messenger_user: messenger_user)
      when 'SKINCARE_RANGE_ANTI_AGING'      then Chatbot::Payloads::SkincareRangeAntiAging.replies(message_obj: message_obj, messenger_user: messenger_user)
      when 'SKINCARE_RANGE_OILY_SKIN'       then Chatbot::Payloads::SkincareRangeOilySkin.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'SKINCARE_RANGE_PAYLOAD2'       then Chatbot::Payloads::SkincareRange2.replies(message_obj: message_obj, messenger_user: messenger_user)

    when 'CATEGORIES_PAYLOAD'           then Chatbot::Payloads::Categories.replies(message_obj: message_obj, messenger_user: messenger_user)

    when 'PRICE_PAYLOAD'                then Chatbot::Payloads::Price.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'CALL_ON_COLLAGEN_PAYLOAD'     then Chatbot::Payloads::CallOnCollagen.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'CHAT_WITH_PAYLOAD'            then Chatbot::Payloads::ChatWith.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'WHAT_IS_COLLAGEN_PAYLOAD'     then Chatbot::Payloads::WhatIsCollagen.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'PENTA_LUCENT_SYSTEM_PAYLOAD'  then Chatbot::Payloads::PentaLucentSystem.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'REJUVENATION_PROGRAM_PAYLOAD' then Chatbot::Payloads::RejuvenationProgram.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'SMART_WATER_CHANNEL_PAYLOAD'  then Chatbot::Payloads::SmartWaterChannel.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'INGESTIBLES_DRINK_PAYLOAD'    then Chatbot::Payloads::IngestiblesDrink.replies(message_obj: message_obj, messenger_user: messenger_user)

    when 'CHECK_PAYLOAD' then [ {text: "Nice test"} ]
    else

      # require "pry"
      # binding.pry

      []
    end
  end

  def self.block_name(payload:)
    case payload
    when 'GET_STARTED_PAYLOAD'          then "Welcome Message"
    when 'SKIN_CONCERNS_PAYLOAD'        then "Skin Concerns"
    when 'OILY_SKIN_PAYLOAD'              then "Oily Skin"
      when 'OILY_SKIN_HYDRO_BALANCE_PAYLOAD'              then "Oily Skin > Hydro Balance"
      when 'OILY_SKIN_NOURISHING_PAYLOAD'                 then "Oily Skin > Nourishing"
      when 'OILY_SKIN_WHITE_REGENERATION_PAYLOAD'         then "Oily Skin > White Regeneration"

    when 'DRY_SKIN_PAYLOAD'               then "Dry Skin"
      when 'DRY_SKIN_NOURISHING_PAYLOAD'                  then "Dry Skin > Nourishing"
      when 'DRY_SKIN_HYDRO_BALANCE_PAYLOAD'               then "Dry Skin > Hydro Balance"

      when 'DARK_SPOTS_PAYLOAD'             then "Dark Spots"
        when 'DARK_SPOTS_WHITE_REGENERATION_PAYLOAD'        then "Dark Spots > White Regeneration"
        when 'DARK_SPOTS_NOURISHING_PAYLOAD'                then "Dark Spots > Nourishing"

      when 'SAGGING_SKIN_PAYLOAD'           then "Sagging Skin"
        when 'SAGGING_SKIN_SKIN_RENEW_PAYLOAD'              then "Sagging Skin > Skin Renew"
        when 'SAGGING_SKIN_NOURISHING_PAYLOAD'              then "Sagging Skin > Nourishing"

      when 'LINES_AND_WRINKLES_PAYLOAD'     then "Lines and Wrinkles"
        when 'LINES_AND_WRINKLES_SKIN_RENEW_PAYLOAD'        then "Lines and Wrinkles > Skin Renew"
        when 'LINES_AND_WRINKLES_NOURISHING_PAYLOAD'        then "Lines and Wrinkles > Nourishing"

      when 'UNEVEN_SKIN_TONE_PAYLOAD'       then "Uneven Skin Tone"
        when 'UNEVEN_SKIN_TONE_WHITE_REGENERATION_PAYLOAD'  then "Uneven Skin Tone > White Regeneration"
        when 'UNEVEN_SKIN_TONE_NOURISHING_PAYLOAD'          then "Uneven Skin Tone > Nourishing"

    when 'BODY_PART_PAYLOAD'            then "Body Part"
      when 'FACE_PAYLOAD'                  then "Face"
        when 'FACE_NOURISHING_PAYLOAD'                      then "Face > Nourishing"
        when 'FACE_WHITE_REGENERATION_PAYLOAD'              then "Face > White Regeneration"
        when 'FACE_SKIN_RENEW_PAYLOAD'                      then "Face > Skin Renew"
        when 'FACE_HYDRO_BALANCE_PAYLOAD'                   then "Face > Hydro Balance"

      when 'HANDS_PAYLOAD'                 then "Hands"
        when 'HANDS_NOURISHING_PAYLOAD'                     then "Hands > Nourishing"

      when 'FEET_PAYLOAD'                 then "Feet"
        when 'FEET_NOURISHING_PAYLOAD'                     then "Feet > Nourishing"

      when 'BODY_PAYLOAD'                 then "Body"
        when 'BODY_NOURISHING_PAYLOAD'                     then "Body > Nourishing"

      when 'EYE_AREA_PAYLOAD'             then "Eye Area"
        when 'EYE_AREA_NOURISHING_PAYLOAD'                 then "Eye Area > Nourishing"
        when 'EYE_AREA_SKIN_RENEW_PAYLOAD'                 then "Eye Area > Skin Renew"
        when 'EYE_AREA_HYDRO_BALANCE_PAYLOAD'              then "Eye Area > Hydro Balance"

      when 'LIPS_PAYLOAD'                 then "Lips"
        when 'LIPS_NOURISHING_PAYLOAD'                     then "Lips > Nourishing"

    when 'REGIMEN_PAYLOAD'                then "Regimen"
    when 'REGIMEN2_PAYLOAD'               then "Regimen"

    when 'PRODUCT_TYPE_PAYLOAD'           then "Product Type"
    when 'PRODUCT_TYPE2_PAYLOAD'          then "Product Type"

    when 'CLEANSING_PAYLOAD'            then "Cleansing"
      when 'CLEANSING_NOURISHING_PAYLOAD'                 then "Cleansing > Nourishing"
      when 'CLEANSING_WHITE_REGENERATION_PAYLOAD'         then "Cleansing > White Regeneration"

      when 'TONING_PAYLOAD'               then "Toning"
        when 'TONING_NOURISHING_PAYLOAD'                    then "Toning > Nourishing"
        when 'TONING_WHITE_REGENERATION_PAYLOAD'            then "Toning > White Regeneration"
        when 'TONING_SKIN_RENEW_PAYLOAD'                    then "Toning > Skin Renew"
        when 'TONING_HYDRO_BALANCE_PAYLOAD'                 then "Toning > Hydro Balance"

      when 'MOISTURISING_PAYLOAD'         then "Moisturising"
        when 'MOISTURISING_NOURISHING_PAYLOAD'              then "Moisturising > Nourishing"
        when 'MOISTURISING_WHITE_REGENERATION_PAYLOAD'      then "Moisturising > White Regeneration"
        when 'MOISTURISING_SKIN_RENEW_PAYLOAD'              then "Moisturising > Skin Renew"
        when 'MOISTURISING_HYDRO_BALANCE_PAYLOAD'           then "Moisturising > Hydro Balance"

      when 'COSMETIC_CREAMS_PAYLOAD'      then "Cosmetic Creams"
        when 'COSMETIC_CREAMS_NOURISHING_PAYLOAD'           then "Cosmetic Creams > Nourishing"
        when 'COSMETIC_CREAMS_WHITE_REGENERATION_PAYLOAD'   then "Cosmetic Creams > White Regeneration"

      when 'SPECIAL_CARE_PAYLOAD'         then "Special Care"
        when 'SPECIAL_CARE_NOURISHING_PAYLOAD'              then "Special Care > Nourishing"
        when 'SPECIAL_CARE_WHITE_REGENERATION_PAYLOAD'      then "Special Care > White Regeneration"
        when 'SPECIAL_CARE_SKIN_RENEW_PAYLOAD'              then "Special Care > Skin Renew"
        when 'SPECIAL_CARE_HYDRO_BALANCE_PAYLOAD'           then "Special Care > Hydro Balance"

    when 'SKINCARE_RANGE_PAYLOAD'       then "Skincare Range"
      when 'SKINCARE_RANGE_MOISTURISING'    then "Skincare Range > Moisturising"
      when 'SKINCARE_RANGE_MOISTURISING_2'  then "Skincare Range > Moisturising"

      when 'SKINCARE_RANGE_WHITENING'       then "Skincare Range > Whitening"
      when 'SKINCARE_RANGE_ANTI_AGING'      then "Skincare Range > Anti-aging"
      when 'SKINCARE_RANGE_OILY_SKIN'       then "Skincare Range > Oily skin"
    when 'SKINCARE_RANGE_PAYLOAD2'       then "Skincare Range"

    when 'CATEGORIES_PAYLOAD'           then "Categories"

    when 'PRICE_PAYLOAD'                then "Product Price"
    when 'CALL_ON_COLLAGEN_PAYLOAD'     then "Call on collagen"
    when 'CHAT_WITH_PAYLOAD'            then "Chat with staff"
    when 'WHAT_IS_COLLAGEN_PAYLOAD'     then "What is collagen"
    when 'PENTA_LUCENT_SYSTEM_PAYLOAD'  then "Penta Lucent System"
    when 'REJUVENATION_PROGRAM_PAYLOAD' then "Skin Rejuvenation Program"
    when 'SMART_WATER_CHANNEL_PAYLOAD'  then "Smart Water Channel"
    when 'INGESTIBLES_DRINK_PAYLOAD'    then "Take-in collagens"

    when 'CHECK_PAYLOAD' then "Test Check"
    else
      nil
    end
  end

end
