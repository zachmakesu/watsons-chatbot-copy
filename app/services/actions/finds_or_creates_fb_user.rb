module Actions
  class FindsOrCreatesFbUser
    extend ::LightService::Action
    expects :email, :first_name, :last_name, :uid
    promises :user

    executed do |ctx|
      if user = User.find_by(email: [ctx.email,"#{ctx.uid}@facebook.com"])
        user.increment!(:sign_in_count)
        ctx.user = user
      else
        user = User.new(
          first_name: ctx.first_name,
          last_name: ctx.last_name,
          email: ctx.email,
          password: Devise.friendly_token[0,20]
        )
        if user.save
          ctx.user = user
        else
          ctx.fail!("Cannot create user account for FB user")
        end
      end
    end
  end
end
