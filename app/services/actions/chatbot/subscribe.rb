module Actions
  module Chatbot
    class Subscribe
      include Facebook::Messenger

      extend ::LightService::Action
      expects :bot_record

      executed do |context|
        @bot_record = context.bot_record
        Facebook::Messenger::Subscriptions.subscribe(access_token: @bot_record.access_token)
      end

    end
  end
end
