module Actions
  module Chatbot

    class CreateSource
      extend LightService::Action

      expects :messenger_user, :message_obj, :fb_page

      executed do |context|
        @context        = context
        @messenger_user = context.messenger_user
        @message_obj    = context.message_obj
        @fb_page        = context.fb_page
        @fb_obj_id      = @message_obj.sender.fetch("id")

        if @message_obj.referral
          ref_params = {
            ref: @message_obj.referral.ref,
            ref_source: @message_obj.referral.source,
            ref_type: @message_obj.referral.type,
            fb_obj_id: @fb_obj_id
          }
          @fb_page.sources.referral.create(ref_params)
        end

      end

    end

  end
end
