module Actions
  module Chatbot

    class CreateOptin
      extend LightService::Action

      expects :messenger_user, :message_obj, :fb_page

      executed do |context|
        @context        = context
        @messenger_user = context.messenger_user
        @message_obj    = context.message_obj
        @fb_page        = context.fb_page
        @fb_obj_id      = @message_obj.sender.fetch("id")
        @fb_page.sources.optin.create(ref: @message_obj.ref, fb_obj_id: @fb_obj_id)
      end

    end

  end
end
