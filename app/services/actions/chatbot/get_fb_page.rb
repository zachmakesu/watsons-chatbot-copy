module Actions
  module Chatbot

    class GetFbPage
      extend LightService::Action

      expects :message_obj
      promises :fb_page

      executed do |context|
        @context     = context
        @message_obj = context.message_obj
        context.fb_page = fb_page
      end

      def self.fb_page
        BotRecord.find_by(page_id: @message_obj.recipient["id"])
      end
    end

  end
end
