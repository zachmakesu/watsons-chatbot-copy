module Actions
  module Chatbot

    class CreateUserActivity
      extend LightService::Action

      expects :messenger_user, :message_obj, :fb_page

      executed do |context|
        @context        = context
        @messenger_user = context.messenger_user
        @message_obj    = context.message_obj
        @fb_page        = context.fb_page

        incoming = @message_obj.class.name.split('::').last
        @fb_obj_id = @message_obj.sender.fetch("id")
        @fb_page.user_activities.create(fb_obj_id: @fb_obj_id, category: incoming)
      end

    end

  end
end
