module Actions
  module Chatbot

    class CreatePayloadWorker
      extend LightService::Action

      expects :messenger_user

      executed do |context|
        @context        = context
        @messenger_user = context.messenger_user
        create_payload_worker! if context.messenger_user.sendable
      end

      def self.create_payload_worker!
        job_id = ChatbotMessenger::AfterPayloadWorker.perform_at(DateTime.now + 2.minutes, @messenger_user.id)
        @messenger_user.update(last_reply: DateTime.now, job_id: job_id)
      end
    end

  end
end
