module Actions
  module Chatbot

    class FetchFbUser
      extend LightService::Action

      expects :message_obj, :fb_page
      promises :fbuser_obj

      executed do |context|
        @context     = context
        @message_obj = context.message_obj
        @fb_page     = context.fb_page
        context.fbuser_obj = fbuser_obj
      end

      def self.fbuser_obj
        messenger_user = MessengerUser.find_by(user_fb_id: @message_obj.sender.fetch("id"))
        if messenger_user
          {
            "first_name"=>messenger_user.first_name,
            "last_name"=>messenger_user.last_name,
            "profile_pic"=>"",
            "locale"=>"en_US",
            "timezone"=>8,
            "gender"=>"",
            "id"=>messenger_user.user_fb_id
          }
        else
          # to prevent page rate limiting of facebook api. Due to calling of api using page access_token
          graph = Koala::Facebook::API.new(@fb_page.access_token)
          graph.get_object( @message_obj.sender.fetch("id"))
        end

      end
    end

  end
end
