module Actions
  module Chatbot

    class NormalMessage
      extend LightService::Action

      expects :message_obj, :messenger_user, :fb_page, :replies
      promises :replies

      executed do |context|
        @context        = context
        @message_obj    = context.message_obj
        @messenger_user = context.messenger_user
        @fb_page        = context.fb_page
        context.replies = replies
      end

      def self.replies
        return @context.replies unless @context.replies.empty?

        @fb_obj_id = @message_obj.sender.fetch("id")
        content = @message_obj.text

        if @messenger_user.message_concern
          ::Chatbot::MessageConcernHandler.response_from(message_obj: @message_obj, messenger_user: @messenger_user)
        else
          payload_obj = AiPayloader.get_payload(text: content)
          if payload_obj
            @fb_page.popularities.user_input.create(content: content, fb_obj_id: @fb_obj_id)
            ::Chatbot::PayloadHandler.response_from(message_obj: @message_obj, payload: payload_obj.payload, messenger_user: @messenger_user)
          else
            @fb_page.popularities.unrecognized_user_input.create(content: content, fb_obj_id: @fb_obj_id)
            ::Chatbot::Payloads::Random.replies(message_obj: @message_obj, messenger_user: @messenger_user)
          end
        end
      end

    end

  end
end
