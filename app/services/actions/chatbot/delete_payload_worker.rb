module Actions
  module Chatbot

    class DeletePayloadWorker
      extend LightService::Action

      expects :messenger_user

      executed do |context|
        @context        = context
        @messenger_user = context.messenger_user
        delete_payload_wodker!
      end

      def self.delete_payload_wodker!
        Sidekiq::ScheduledSet.new.select{|j| j.jid == @messenger_user.job_id }.each(&:delete) if @messenger_user.job_id
      end
    end

  end
end
