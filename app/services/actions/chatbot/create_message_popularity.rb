module Actions
  module Chatbot

    class CreateMessagePopularity
      extend LightService::Action

      expects :messenger_user, :message_obj, :fb_page

      executed do |context|
        @context        = context
        @messenger_user = context.messenger_user
        @message_obj    = context.message_obj
        @fb_page        = context.fb_page
        @fb_obj_id      = @message_obj.sender.fetch("id")
        payload         = @message_obj.quick_reply
        content         = @message_obj.text

        @block_name = ::Chatbot::PayloadHandler.block_name(payload: payload)

        @fb_page.popularities.button.create(content: content, name: @block_name, block_payload: payload, fb_obj_id: @fb_obj_id)
        @fb_page.popularities.block.create(content: content, name: @block_name, block_payload: payload, fb_obj_id: @fb_obj_id)
      end

    end

  end
end
