module Actions
  module Chatbot

    class CreatePostbackPopularity
      extend LightService::Action

      expects :messenger_user, :message_obj, :fb_page, :block_name

      executed do |context|
        @context        = context
        @messenger_user = context.messenger_user
        @message_obj    = context.message_obj
        @fb_page        = context.fb_page
        @fb_obj_id      = @message_obj.sender.fetch("id")
        @block_name     = context.block_name

        payload = @message_obj.payload
        content = @message_obj.messaging["postback"]["title"]

        @fb_page.popularities.button.create(content: content, name: @block_name, block_payload: payload, fb_obj_id: @fb_obj_id)
        @fb_page.popularities.block.create(content: content, name: @block_name, block_payload: payload, fb_obj_id: @fb_obj_id)
      end

    end

  end
end
