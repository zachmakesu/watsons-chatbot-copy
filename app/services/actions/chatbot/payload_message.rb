module Actions
  module Chatbot

    class PayloadMessage
      extend LightService::Action

      expects :message_obj, :messenger_user
      promises :replies, :block_name

      executed do |context|
        @context        = context
        @message_obj    = context.message_obj
        @messenger_user = context.messenger_user
        context.replies = replies
        context.block_name = block_name
      end

      def self.replies
        payload = @message_obj.payload
        ::Chatbot::PayloadHandler.response_from(message_obj: @message_obj, payload: payload, messenger_user: @messenger_user)
      end

      def self.block_name
        payload = @message_obj.payload
        ::Chatbot::PayloadHandler.block_name(payload: payload)
      end
    end

  end
end
