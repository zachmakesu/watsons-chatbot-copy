module Actions
  module Chatbot

    class QuickReplyMessage
      extend LightService::Action

      expects :message_obj, :messenger_user, :fb_page
      promises :replies

      executed do |context|
        @context        = context
        @message_obj    = context.message_obj
        @messenger_user = context.messenger_user
        @fb_page        = context.fb_page
        @fb_obj_id      = @message_obj.sender.fetch("id")

        context.replies = replies
      end

      def self.replies
        return [] unless @message_obj.messaging["message"]["quick_reply"]
        @messenger_user.update(message_concern: nil)
        payload = @message_obj.messaging["message"]["quick_reply"]["payload"]
        content = @message_obj.text

        @block_name = ::Chatbot::PayloadHandler.block_name(payload: payload)
        @fb_page.popularities.button.create(content: content, name: @block_name, block_payload: payload, fb_obj_id: @fb_obj_id)
        @fb_page.popularities.block.create(content: content, name: @block_name, block_payload: payload, fb_obj_id: @fb_obj_id)

        #PORO THAT RETURNS SPECIFIC ARRAYS
        ::Chatbot::PayloadHandler.response_from(message_obj: @message_obj, payload: payload, messenger_user: @messenger_user)
      end

    end

  end
end
