module Actions
  module Session
    class FindOrCreateUser
      extend ::LightService::Action
      expects :auth
      promises :user

      executed do |context|
        @auth = context.auth
        context.user = user
      end

      def self.user
        user = User.find_or_create_by(provider: @auth.provider, uid: @auth.uid) do |user|
          user.uid              = @auth.uid
          user.provider         = @auth.provider
          user.email            = @auth.info.email || "#{auth.uid}@facebook.com"
          user.oauth_token      = @auth.credentials.token
          user.oauth_expires_at = @auth.credentials.expires_at
          user.password         = Devise.friendly_token[0, 20]
          user.first_name       = @auth.info.first_name
          user.last_name        = @auth.info.last_name
          user.image_url        = @auth.info.image
        end
        
        user.update(first_name: @auth.info.first_name, last_name: @auth.info.last_name, image_url: @auth.info.image)
        user
      end
    end
  end
end
