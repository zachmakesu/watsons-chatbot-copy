module Actions
  class UpdatesFbIdentity
    extend ::LightService::Action
    expects :uid, :user, :token

    executed do |ctx|
      ctx.user.update({
        provider: 'facebook',
        oauth_token:  ctx.token,
        oauth_expires_at:  (DateTime.now.utc + 30.days)
      })
    end
  end
end
