module Actions
  class GeneratesAPIKey
    extend ::LightService::Action
    expects :user
    promises :access_token, :uid

    executed do |ctx|
      ctx.user.delete_previous_api_keys
      access_token = SecureRandom.hex(16)
      ctx.user.api_keys.create!(access_token: access_token)
      ctx.access_token = access_token
      ctx.uid = ctx.user.uid
    end
  end
end
