module Actions
  class ValidatesFbToken
    extend ::LightService::Action
    expects :token
    promises :email, :first_name, :last_name, :uid

    executed do |ctx|
      graph = Koala::Facebook::API.new(ctx.token)
      begin
        user = graph.get_object('me?fields=email,first_name,last_name')
        ctx.email       = user.fetch("email"){ "" }
        ctx.first_name  = user.fetch("first_name"){ "" }
        ctx.last_name   = user.fetch("last_name"){ "" }
        ctx.uid         = user.fetch("id"){ "" }
      rescue Koala::KoalaError => e
        ctx.fail!("Facebook access token is invalid")
        next ctx
      end
    end
  end
end
