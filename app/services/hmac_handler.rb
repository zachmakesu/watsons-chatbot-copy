class HmacHandler
  attr_accessor :endpoint, :params, :version
  WHITELISTED_ATTR = %w{
    avatar
  }

  # https://instagram.com/developer/secure-api-requests/?hl=en
  def self.signature_from(endpoint, params, api_version=1)
    secret = if api_version == 1
               ENV['HMAC_SECRET']
             else
               ENV["HMAC_SECRET_#{api_version}"]
             end
    sig = endpoint
    params.sort.map do |key, val|
      next if WHITELISTED_ATTR.include?(key)
      sig += '|%s=%s' % [key, val]
    end
    digest = OpenSSL::Digest.new('sha256')
    # Rails.logger.debug "sigstr: #{sig}" if Rails.env.staging?
    return OpenSSL::HMAC.hexdigest(digest, secret, sig)
  end

  # http://www.rubydoc.info/github/plataformatec/devise/Devise.secure_compare
  def self.secure_compare(a, b)
    return false if a.blank? || b.blank? || a.bytesize != b.bytesize
    l = a.unpack "C#{a.bytesize}"

    res = 0
    b.each_byte { |byte| res |= byte ^ l.shift }
    res == 0
  end

  def initialize endpoint, params, version=1
    @endpoint = endpoint
    @params = params
    @version = version
  end

  def digest
    HmacHandler.signature_from(@endpoint, @params, @version)
  end

end


# Vue
# const params = {
#   access_token: payload.access_token,
#   avatar: "asda"
# }

# const endpoint = Vue.http.options.root + 'profile';
# const hmac_secret = '904727dc44987afab8ea9767e1f060730e7a2962f9be1af18c8d40ba6e7052e327472f6d36fd90c31942d7f4532acb00f0f0ad54040d3eec002f4dee404bee78';



# Rails

# params = {
#   access_token: "asda",
#   avatar: "asda",
#   test: "test"
# }

# sig = "http://localhost:3000/api/v1/profile"

# whitelisted = %w{ avatar test }

# params.sort.map do |key, val|
# next if whitelisted.include?(key)
# sig += '|%s=%s' % [key, val]
# end

# sig will be like this ["http://localhost:3000/api/v1/profile|access_token=asda", "http://localhost:3000/api/v1/profile|access_token=asda|avatar=asda"]


# digest = OpenSSL::Digest.new('sha256')

# return OpenSSL::HMAC.hexdigest(digest, secret, sig)