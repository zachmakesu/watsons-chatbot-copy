class AuthenticateFbUser
  extend LightService::Organizer

  def self.call(token: "")
    with(
      token: token
    ).reduce(
      Actions::ValidatesFbToken,
      Actions::FindsOrCreatesFbUser,
      Actions::UpdatesFbIdentity,
      Actions::GeneratesAPIKey
    )
  end
end
