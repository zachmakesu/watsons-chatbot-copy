module FormatHelper

  def month_day_format(d)
    d.try(:strftime,"%b\n%d")
  end

  def month_day_format_2(d)
    d.try(:strftime,"%B\n%d")
  end

  def day_format(d)
    d.try(:strftime,"%a")
  end

  def day_format_2(d)
    d.try(:strftime,"%d")
  end

  def month_day_year_format(d)
    d.try(:strftime,"%B %d, %Y")
  end

  def time_format(d)
    d.try(:strftime,"%I:%M %p")
  end

  def complete_date_format(d)
    d.try(:strftime,"%B %d, %Y %I:%M %p %z")
  end

  def date_time_format(d)
    d.try(:strftime,"%B %d, %Y %I:%M %p")
  end

  def currency_format(value,unit='')
    number_to_currency(value, unit: unit)
  end

  def generate_country_options
    CS.countries.map{|c| [c[1],c[0]]}
  end

  def generate_city_options
    CS.cities("00","PH")
  end

  def date_picker_format(d)
    d.try(:strftime,"%m-%d-%Y")
  end

  def month_year_format(d)
    d.try(:strftime, "%m/%Y")
  end
end
