module AnalyticsHelper

  def generate_content_for(block)
    if block.first[:block_payload].present?
      splitted_payload = block.first[:block_payload].split("_")
      splitted_payload.reverse.drop(1).reverse.join("_")
    else
      block.first[:content]
    end
  end

  def daily_users_actions_chart
    line_chart daily_users_actions_path(start_ts: @start_ts, end_ts: @end_ts)
  end

  def daily_new_users_chart
    line_chart daily_new_users_path(start_ts: @start_ts, end_ts: @end_ts)
  end

  def user_activity_chart
    line_chart user_activity_path(start_ts: @start_ts, end_ts: @end_ts)
  end

  # def sources_chart
  #   area_chart sources_path(start_ts: @start_ts, end_ts: @end_ts)
  # end
end
