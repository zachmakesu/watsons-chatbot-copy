source "https://rubygems.org"

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem "active_type", ">= 0.3.2"
gem "autoprefixer-rails", ">= 5.0.0.1"
gem "bcrypt", "~> 3.1.7"
gem "bootsnap", ">= 1.1.0", :require => false
gem "coffee-rails", "~> 4.2"
gem "dotenv-rails", ">= 2.0.0"
gem "jquery-rails"
gem "mail", ">= 2.6.3"
gem "marco-polo"
gem "pg", "~> 0.18"
gem "pgcli-rails"
gem "rails", "5.1.3"
gem "redis-namespace"
gem "sass-rails", "~> 5.0"
gem "secure_headers", "~> 3.0"
gem "sidekiq", ">= 4.2.0"
gem 'rspec-rails'
gem 'ci_reporter_rspec'
gem 'grape'
gem 'grape-entity'
gem 'grape_on_rails_routes'
gem "sinatra", ">= 1.3.0", :require => false
gem 'faker'
gem 'draper'
gem 'foundation-rails'
gem 'devise'
gem "omniauth-facebook", "~> 4.0.0"
gem "koala", "~> 3.0.0"
gem 'carrierwave', '~> 1.0'
gem 'simple_form'
gem 'kaminari'
gem 'simple-navigation', '~> 4.0', '>= 4.0.3'
gem 'tabs_on_rails'
gem 'toastr-rails'
gem 'jquery-ui-rails'
gem "facebook-messenger"
# gem "webpacker", "~> 2.0"
gem "active_model_serializers"
gem "uglifier"
gem "foundation-icons-sass-rails"
gem 'light-service'
gem 'ionicons-rails', '~> 2.0'
gem 'font-awesome-sass'
gem 'chartkick', '~> 2.2.0'

group :production, :staging do
  gem "postmark-rails"
  gem "unicorn"
  gem "unicorn-worker-killer"
end

group :development do
  gem "annotate", ">= 2.5.0"
  gem "awesome_print"
  gem "bcrypt_pbkdf", :require => false
  gem "better_errors"
  gem "binding_of_caller"
  gem "brakeman", :require => false
  gem "bundler-audit", ">= 0.5.0", :require => false
  gem "capistrano", "~> 3.6", :require => false
  gem "capistrano-bundler", "~> 1.2", :require => false
  gem "capistrano-mb", ">= 0.22.2", :require => false
  gem "capistrano-nc", :require => false
  gem "capistrano-rails", :require => false
  gem "capistrano-db-tasks", require: false
  gem "guard", ">= 2.2.2", :require => false
  gem "guard-livereload", :require => false
  gem "guard-minitest", :require => false
  gem "letter_opener"
  gem "listen", ">= 3.0.5"
  gem "overcommit", ">= 0.37.0", :require => false
  gem "rack-livereload"
  gem "rb-fsevent", :require => false
  gem "rbnacl", "~> 3.4", :require => false
  gem "rbnacl-libsodium", :require => false
  gem "rubocop", ">= 0.44.0", :require => false
  gem "simplecov", :require => false
  gem "spring"
  gem "sshkit", "~> 1.8", :require => false
  gem "spring-watcher-listen", "~> 2.0.0"
  gem "terminal-notifier", :require => false
  gem "terminal-notifier-guard", :require => false
  gem "xray-rails", ">= 0.1.18"
end

group :test do
  gem "vcr"
  gem "webmock"
  gem 'shoulda-matchers'
  gem "shoulda-context"
  gem "capybara"
  gem "connection_pool"
  gem "launchy"
  gem "mocha"
  gem "poltergeist"
  gem "test_after_commit"
  gem "factory_girl_rails"
  gem 'database_cleaner'
  gem "pusher-fake", "1.7.0"
end

group :development, :test do
  gem "bullet"
  gem "pry"
end
