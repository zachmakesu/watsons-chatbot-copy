users = [
  {
    email: "admin@gorated.ph",
    first_name: "Super Sayan",
    last_name: "Admin",
    password: "password123",
    role: "super_admin",
    image_url: "https://www.lawfarm.in/assets/default-avatar-25d12326da7a8a5adc9a191e63d512e3c8345d1ab03eacbf9a89f24f2ce7f1c6.png"
  }
]
ActiveRecord::Base.transaction do
  users.each do |user|
    new_user = User.new
    new_user.email       = user[:email]
    new_user.first_name  = user[:first_name]
    new_user.last_name   = user[:last_name]
    new_user.last_name   = user[:last_name]
    new_user.role        = user[:role]
    new_user.password    = user[:password]
    new_user.image_url   = user[:image_url]
    if new_user.save
      print '✓'
    else
      puts new_user.errors.inspect
    end
  end
  print "\nTotal : #{User.all.count}\n"
end
