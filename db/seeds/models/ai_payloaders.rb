ai_payloaders = [
  {
    keyword: "why does skin develop dark spots",
    payload: "DARK_SPOTS_PAYLOAD"
  },
  {
    keyword: "dark spot",
    payload: "DARK_SPOTS_PAYLOAD"
  },
  {
    keyword: "how much",
    payload: "PRICE_PAYLOAD"
  },
  {
    keyword: "how much?",
    payload: "PRICE_PAYLOAD"
  },
  {
    keyword: "magkano?",
    payload: "PRICE_PAYLOAD"
  },
  {
    keyword: "magkano",
    payload: "PRICE_PAYLOAD"
  },
  {
    keyword: "price?",
    payload: "PRICE_PAYLOAD"
  },
  {
    keyword: "price",
    payload: "PRICE_PAYLOAD"
  },
  {
    keyword: "hm?",
    payload: "PRICE_PAYLOAD"
  },
  {
    keyword: "hm",
    payload: "PRICE_PAYLOAD"
  },
  {
    keyword: "order ako",
    payload: "PRICE_PAYLOAD"
  },
  {
    keyword: "pa order",
    payload: "PRICE_PAYLOAD"
  },
  {
    keyword: "wrinkles",
    payload: "LINES_AND_WRINKLES_PAYLOAD"
  },
  {
    keyword: "oily skin",
    payload: "OILY_SKIN_PAYLOAD"
  },
  {
    keyword: "oily",
    payload: "OILY_SKIN_PAYLOAD"
  },
  {
    keyword: "skin concern",
    payload: "SKIN_CONCERNS_PAYLOAD"
  },
  {
    keyword: "concern",
    payload: "SKIN_CONCERNS_PAYLOAD"
  },
  {
    keyword: "tri-collagen",
    payload: "SKINCARE_RANGE_PAYLOAD"
  },
  {
    keyword: "dry skin",
    payload: "DRY_SKIN_PAYLOAD"
  },
  {
    keyword: "white",
    payload: "SKINCARE_RANGE_WHITENING"
  },
  {
    keyword: "whiten",
    payload: "SKINCARE_RANGE_WHITENING"
  },
  {
    keyword: "whitening",
    payload: "SKINCARE_RANGE_WHITENING"
  },
  {
    keyword: "anti-aging",
    payload: "SKINCARE_RANGE_ANTI_AGING"
  },
  {
    keyword: "aging",
    payload: "SKINCARE_RANGE_ANTI_AGING"
  },
  {
    keyword: "hydrobalance",
    payload: "SKINCARE_RANGE_OILY_SKIN"
  },
  {
    keyword: "hydro balance",
    payload: "SKINCARE_RANGE_OILY_SKIN"
  },
  {
    keyword: "hydro",
    payload: "SKINCARE_RANGE_OILY_SKIN"
  },
  {
    keyword: "chat with",
    payload: "CHAT_WITH_PAYLOAD"
  },
  {
    keyword: "chat to",
    payload: "CHAT_WITH_PAYLOAD"
  },
  {
    keyword: "talk to",
    payload: "CHAT_WITH_PAYLOAD"
  },
  {
    keyword: "talk with",
    payload: "CHAT_WITH_PAYLOAD"
  },
  {
    keyword: "who",
    payload: "CHAT_WITH_PAYLOAD"
  },
  {
    keyword: "face",
    payload: "FACE_PAYLOAD"
  },
  {
    keyword: "faces",
    payload: "FACE_PAYLOAD"
  },
  {
    keyword: "mask",
    payload: "FACE_PAYLOAD"
  },
  {
    keyword: "masks",
    payload: "FACE_PAYLOAD"
  },
  {
    keyword: "hand",
    payload: "HANDS_PAYLOAD"
  },
  {
    keyword: "hand cream",
    payload: "HANDS_PAYLOAD"
  },
  {
    keyword: "hands",
    payload: "HANDS_PAYLOAD"
  },
  {
    keyword: "feet",
    payload: "FEET_PAYLOAD"
  },
  {
    keyword: "foot",
    payload: "FEET_PAYLOAD"
  },
  {
    keyword: "heel",
    payload: "FEET_PAYLOAD"
  },
  {
    keyword: "heel cream",
    payload: "FEET_PAYLOAD"
  },
  {
    keyword: "heels",
    payload: "FEET_PAYLOAD"
  },
  {
    keyword: "body",
    payload: "BODY_PAYLOAD"
  },
  {
    keyword: "what do I use for my body?",
    payload: "BODY_PAYLOAD"
  },
  {
    keyword: "what collagen product is good for the eye?",
    payload: "EYE_AREA_PAYLOAD"
  },
  {
    keyword: "eye",
    payload: "EYE_AREA_PAYLOAD"
  },
  {
    keyword: "eyes",
    payload: "EYE_AREA_PAYLOAD"
  },
  {
    keyword: "eye area",
    payload: "EYE_AREA_PAYLOAD"
  },
  {
    keyword: "lip",
    payload: "LIPS_PAYLOAD"
  },
  {
    keyword: "lips",
    payload: "LIPS_PAYLOAD"
  },
  {
    keyword: "nourishing",
    payload: "SKINCARE_RANGE_MOISTURISING"
  },
  {
    keyword: "moisturising",
    payload: "SKINCARE_RANGE_MOISTURISING"
  },
  {
    keyword: "moisturizing",
    payload: "SKINCARE_RANGE_MOISTURISING"
  },
  {
    keyword: "whitening",
    payload: "SKINCARE_RANGE_WHITENING"
  },
  {
    keyword: "white",
    payload: "SKINCARE_RANGE_WHITENING"
  },
  {
    keyword: "anti-aging",
    payload: "SKINCARE_RANGE_ANTI_AGING"
  },
  {
    keyword: "anti-ageing",
    payload: "SKINCARE_RANGE_ANTI_AGING"
  },
  {
    keyword: "anti aging",
    payload: "SKINCARE_RANGE_ANTI_AGING"
  },
  {
    keyword: "anti ageing",
    payload: "SKINCARE_RANGE_ANTI_AGING"
  },
  {
    keyword: "skin renew",
    payload: "SKINCARE_RANGE_ANTI_AGING"
  },
  {
    keyword: "oily skin",
    payload: "SKINCARE_RANGE_OILY_SKIN"
  },
  {
    keyword: "tri-collagen",
    payload: "SKINCARE_RANGE_PAYLOAD"
  },
  {
    keyword: "tricollagen",
    payload: "SKINCARE_RANGE_PAYLOAD"
  },
  {
    keyword: "tri collagen",
    payload: "SKINCARE_RANGE_PAYLOAD"
  },
  {
    keyword: "collagen",
    payload: "SKINCARE_RANGE_PAYLOAD"
  },
  {
    keyword: "call on collagen",
    payload: "CALL_ON_COLLAGEN_PAYLOAD"
  },
  {
    keyword: "what is collagen?",
    payload: "WHAT_IS_COLLAGEN_PAYLOAD"
  },
  {
    keyword: "what is collagen",
    payload: "WHAT_IS_COLLAGEN_PAYLOAD"
  },
  {
    keyword: "what is penta lucent system?",
    payload: "PENTA_LUCENT_SYSTEM_PAYLOAD"
  },
  {
    keyword: "what is penta lucent system",
    payload: "PENTA_LUCENT_SYSTEM_PAYLOAD"
  },
  {
    keyword: "penta lucent system",
    payload: "PENTA_LUCENT_SYSTEM_PAYLOAD"
  },
  {
    keyword: "penta lucent",
    payload: "PENTA_LUCENT_SYSTEM_PAYLOAD"
  },
  {
    keyword: "what is the skin rejuvenation program?",
    payload: "REJUVENATION_PROGRAM_PAYLOAD"
  },
  {
    keyword: "what is the skin rejuvenation program",
    payload: "REJUVENATION_PROGRAM_PAYLOAD"
  },
  {
    keyword: "skin rejuvenation",
    payload: "REJUVENATION_PROGRAM_PAYLOAD"
  },
  {
    keyword: "skin rejuvenation program",
    payload: "REJUVENATION_PROGRAM_PAYLOAD"
  },
  {
    keyword: "what is smart water channel?",
    payload: "SMART_WATER_CHANNEL_PAYLOAD"
  },
  {
    keyword: "what is smart water channel",
    payload: "SMART_WATER_CHANNEL_PAYLOAD"
  },
  {
    keyword: "smart water channel",
    payload: "SMART_WATER_CHANNEL_PAYLOAD"
  },
  {
    keyword: "smart water",
    payload: "SMART_WATER_CHANNEL_PAYLOAD"
  },
  {
    keyword: "do you have collagen ingestibles or ready-to-drink collagen?",
    payload: "INGESTIBLES_DRINK_PAYLOAD"
  },
  {
    keyword: "do you have collagen ingestibles or ready-to-drink collagen",
    payload: "INGESTIBLES_DRINK_PAYLOAD"
  },
  {
    keyword: "collagen ingestibles",
    payload: "INGESTIBLES_DRINK_PAYLOAD"
  },
  {
    keyword: "ready-to-drink collagen",
    payload: "INGESTIBLES_DRINK_PAYLOAD"
  },
  {
    keyword: "ready to drink collagen",
    payload: "INGESTIBLES_DRINK_PAYLOAD"
  },
  {
    keyword: "cleansers",
    payload: "CLEANSING_PAYLOAD"
  },
  {
    keyword: "cleanser",
    payload: "CLEANSING_PAYLOAD"
  },
  {
    keyword: "facial wash",
    payload: "CLEANSING_PAYLOAD"
  },
  {
    keyword: "moisturiser",
    payload: "MOISTURISING_PAYLOAD"
  },
  {
    keyword: "moisturisers",
    payload: "MOISTURISING_PAYLOAD"
  },
  {
    keyword: "moisturizers",
    payload: "MOISTURISING_PAYLOAD"
  },
  {
    keyword: "moisturizer",
    payload: "MOISTURISING_PAYLOAD"
  },
  {
    keyword: "facial cream",
    payload: "MOISTURISING_PAYLOAD"
  },
  {
    keyword: "cream",
    payload: "MOISTURISING_PAYLOAD"
  },
  {
    keyword: "creams",
    payload: "MOISTURISING_PAYLOAD"
  },
  {
    keyword: "toners",
    payload: "TONING_PAYLOAD"
  },
  {
    keyword: "toner",
    payload: "TONING_PAYLOAD"
  },
  {
    keyword: "serum",
    payload: "SPECIAL_CARE_PAYLOAD"
  },
  {
    keyword: "serum",
    payload: "SPECIAL_CARE_PAYLOAD"
  },
  {
    keyword: "special care",
    payload: "SPECIAL_CARE_PAYLOAD"
  },
  {
    keyword: "complexion corrector cream",
    payload: "COSMETIC_CREAMS_PAYLOAD"
  },
  {
    keyword: "cosmetic creams",
    payload: "COSMETIC_CREAMS_PAYLOAD"
  },
  {
    keyword: "instant finishing cream",
    payload: "COSMETIC_CREAMS_PAYLOAD"
  },
  {
    keyword: "cc cream",
    payload: "COSMETIC_CREAMS_PAYLOAD"
  },
  {
    keyword: "cc",
    payload: "COSMETIC_CREAMS_PAYLOAD"
  },
]
ActiveRecord::Base.transaction do
  ai_payloaders.each do |ai_payloader|
    new_ai_payloader = AiPayloader.new
    new_ai_payloader.keyword = ai_payloader[:keyword]
    new_ai_payloader.payload  = ai_payloader[:payload]
    if new_ai_payloader.save
      print '✓'
    else
      puts new_ai_payloader.errors.inspect
    end
  end
  print "\nTotal : #{AiPayloader.all.count}\n"
end
