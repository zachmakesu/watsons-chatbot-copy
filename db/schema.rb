# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180105122229) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ai_payloaders", force: :cascade do |t|
    t.text "keyword", null: false
    t.string "payload", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "api_keys", force: :cascade do |t|
    t.bigint "user_id"
    t.string "encrypted_access_token", default: "", null: false
    t.datetime "expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_api_keys_on_user_id"
  end

  create_table "bot_managements", force: :cascade do |t|
    t.bigint "bot_record_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bot_record_id"], name: "index_bot_managements_on_bot_record_id"
    t.index ["user_id"], name: "index_bot_managements_on_user_id"
  end

  create_table "bot_records", force: :cascade do |t|
    t.text "verify_token"
    t.string "page_id"
    t.string "name"
    t.text "access_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messenger_users", force: :cascade do |t|
    t.bigint "user_fb_id", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "job_id"
    t.datetime "last_reply"
    t.boolean "sendable", default: true
    t.string "message_concern"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "popular_urls", force: :cascade do |t|
    t.bigint "bot_record_id"
    t.string "name", null: false
    t.string "registered_link", null: false
    t.bigint "fb_obj_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bot_record_id"], name: "index_popular_urls_on_bot_record_id"
  end

  create_table "popularities", force: :cascade do |t|
    t.integer "category", null: false
    t.bigint "fb_obj_id", null: false
    t.string "block_payload"
    t.text "content", null: false
    t.bigint "bot_record_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["bot_record_id"], name: "index_popularities_on_bot_record_id"
  end

  create_table "postback_payloads", force: :cascade do |t|
    t.string "payload", null: false
    t.text "send_hash", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "registered_links", force: :cascade do |t|
    t.string "orig_url", null: false
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "content"
  end

  create_table "sources", force: :cascade do |t|
    t.bigint "bot_record_id"
    t.bigint "fb_obj_id", null: false
    t.integer "category", null: false
    t.string "ref", null: false
    t.string "ref_type"
    t.string "ref_source"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bot_record_id"], name: "index_sources_on_bot_record_id"
  end

  create_table "user_activities", force: :cascade do |t|
    t.bigint "bot_record_id"
    t.bigint "fb_obj_id", null: false
    t.string "category", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bot_record_id"], name: "index_user_activities_on_bot_record_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "uid"
    t.string "first_name"
    t.string "last_name"
    t.string "image_url"
    t.string "provider"
    t.text "oauth_token"
    t.integer "oauth_expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role", default: 0
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "api_keys", "users"
  add_foreign_key "bot_managements", "bot_records"
  add_foreign_key "bot_managements", "users"
  add_foreign_key "popular_urls", "bot_records"
end
