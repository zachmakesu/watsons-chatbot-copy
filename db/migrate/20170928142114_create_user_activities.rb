class CreateUserActivities < ActiveRecord::Migration[5.1]
  def change
    create_table :user_activities do |t|
      t.references  :bot_record
      t.integer     :fb_obj_id, null: false, limit: 5
      t.string      :category, null: false
      t.timestamps
    end
  end
end
