class CreatePostbackPayloads < ActiveRecord::Migration[5.1]
  def change
    create_table :postback_payloads do |t|
      t.string :payload, null: false
      t.text   :send_hash, null: false
      t.timestamps
    end
  end
end
