class CreateSources < ActiveRecord::Migration[5.1]
  def change
    create_table :sources do |t|
      t.references  :bot_record
      t.integer     :fb_obj_id, null: false, limit: 5
      t.integer     :category, null: false
      t.string      :ref, null: false
      t.string      :ref_type
      t.string      :ref_source
      t.timestamps
    end
  end
end
