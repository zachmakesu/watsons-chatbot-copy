class UpdateRegisteredurlAttrs < ActiveRecord::Migration[5.1]
  def change
    remove_index :registered_links, column: :orig_url
    add_column   :registered_links, :content, :string
  end
end
