class CreatePopularities < ActiveRecord::Migration[5.1]
  def change
    create_table :popularities do |t|
      t.integer     :category, null: false
      t.integer     :fb_obj_id, null: false, limit: 5
      t.string      :block_payload
      t.text        :content, null: false
      t.references  :bot_record
      t.timestamps
    end
  end
end
