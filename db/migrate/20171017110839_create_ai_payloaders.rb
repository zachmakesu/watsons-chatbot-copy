class CreateAiPayloaders < ActiveRecord::Migration[5.1]
  def change
    create_table :ai_payloaders do |t|
      t.text        :keyword, null: false
      t.string      :payload, null: false
      t.timestamps
    end
  end
end
