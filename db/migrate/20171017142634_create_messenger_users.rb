class CreateMessengerUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :messenger_users do |t|
      t.integer   :user_fb_id, limit: 5, null: false
      t.string    :first_name
      t.string    :last_name
      t.string    :job_id
      t.datetime  :last_reply
      t.boolean   :sendable, default: true
      t.string    :message_concern
      t.timestamps
    end
  end
end
