class CreateBotRecords < ActiveRecord::Migration[5.1]
  def change
    create_table :bot_records do |t|
      t.text :verify_token
      t.string :page_id
      t.string :name
      t.text :access_token
      t.references :user

      t.timestamps
    end
    add_index :bot_records, :page_id, unique: true
  end
end
