class AddNameOnPopularity < ActiveRecord::Migration[5.1]
  def change
    add_column :popularities, :name, :string
  end
end
