class CreatePopularUrls < ActiveRecord::Migration[5.1]
  def change
    create_table :popular_urls do |t|
      t.references :bot_record, index: true, foreign_key: true
      t.string     :name, null: false
      t.string     :registered_link, null: false
      t.integer    :fb_obj_id, null: false, limit: 5
      t.timestamps
    end
  end
end
