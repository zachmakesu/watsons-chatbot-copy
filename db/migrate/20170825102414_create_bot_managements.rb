class CreateBotManagements < ActiveRecord::Migration[5.1]
  def up
    create_table :bot_managements do |t|
      t.references :bot_record, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
    remove_index :bot_records, :page_id
    remove_column :bot_records, :user_id
  end

  def down
    drop_table :bot_managements
    add_column :bot_records, :user_id, :integer
    add_index :bot_records, :page_id, unique: true
  end
end
