class CreateRegisteredLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :registered_links do |t|
      t.string      :orig_url, null: false
      t.string      :title
      t.timestamps
    end

    add_index :registered_links, :orig_url,                unique: true
  end
end
